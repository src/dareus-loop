#!/usr/bin/env python2.7

"""
author: Yasaman Karami
5 July 2018
MTi
"""
###########################
###  L I B R A R I E S  ###
###########################
import sys
import os
from PyPDB import PyPDB as PDB
from PyPDB.Geo3DUtils import *
from optparse import OptionParser
import argparse

DFLT_NPROC         = 8
DFLT_WORK_PATH     = "./"
DFLT_PDB_NAME      = "pdbChainID"
DFLT_LOOP_SEQ      = "AAAAA"
DFLT_PYPPP_PATH    = "./"
PROT_DIR           = "DaReUS_Loop"
DFLT_BANK          = "/scratch/banks/bc_banks"
DFLT_PROF_PATH     = DFLT_BANK + "/PyPPP_profiles/"
DFLT_ALL_PDB_PATH  = DFLT_BANK + "/pdb/"
socketName         = "pyppp3.socket"
######################################
def print_options(options):
    """
    @return: a string describing the options setup for the run
    """
    try:
        rs = ""
        for option in options.__dict__.keys():
            rs = "%s\n  %10s: %s" % (rs, str(option),getattr(options,option))
        return rs
    except:
        return ""


if __name__=="__main__":


    parser = argparse.ArgumentParser()
    
    parser.add_argument("--ppp_path", dest="prof_path", help="path to profiles (%s)" % DFLT_PROF_PATH,
                        action="store", default=DFLT_PROF_PATH)

    parser.add_argument("--pyppp_path", dest="pyppp_path", help="path to PyPPP (%s)" % DFLT_PYPPP_PATH,
                        action="store", default=DFLT_PYPPP_PATH)

    parser.add_argument("--target", dest="target", help="gapped PDB to model the missing region",
                        action="store", required=True, default = None)
    
    parser.add_argument("--wpath", dest="work_path", help="path to target (%s)" % DFLT_WORK_PATH,
                        action="store", default=DFLT_WORK_PATH)

    
    options = parser.parse_args() # Do not pass sys.argv
    # print options.__dict__.keys()
    if options.work_path[-1] != "/":
        options.work_path = options.work_path + "/"
    print print_options(options)

    #    sys.exit(0)

    # input
    Prof_dir   = options.prof_path
    PyPPP3_dir  = options.pyppp_path
    targetAddr = options.work_path
    target     = options.target

    model_dir = targetAddr + PROT_DIR + "/"
    PPP_name = "target_seq"
    PPPfile = open(targetAddr + "runPyPPP.sh", "w")
    #if not os.path.isfile(Prof_dir + PPP_name + ".svmi8.27.prob"):
    fst_name = model_dir + PPP_name + ".fst"
    os.chdir(model_dir)
    PPPfile.write("cd " + model_dir + "\n")
    cmd = "%s/PyPPP3d --2012 --socket %s &" % (PyPPP3_dir, socketName)
    PPPfile.write(cmd + "\n")
    PPPfile.write("sleep 30\n")
    cmd = "%s/PyPPP3c -s %s -l %s --noPlot --noUpper --socket %s" % (PyPPP3_dir, fst_name, PPP_name, socketName)
    PPPfile.write(cmd + "\n")
    cmd = "%s/PyPPP3c -q --socket %s" % (PyPPP3_dir, socketName)
    PPPfile.write(cmd + "\n")
