#!/usr/bin/env python2.7

"""
author: Yasaman Karami
11 December 2018
MTi
"""

###########################
###  L I B R A R I E S  ###
###########################
import numpy as np
import sys, os
import argparse
from PyPDB import PyPDB as PDB
from Bio import pairwise2

DFLT_WORK_PATH     = "./"
DFLT_BANK          = "/scratch/banks/bc_banks"
DFLT_PDB_CLS_PATH  = DFLT_BANK + "/PDB_clusters/"
DFLT_FLANK_SZE     = 4
PROT_DIR           = "DaReUS_Loop"
DFLT_LBL           = "dareus"
DFLT_MODE          = "local"  #else "cluster"
DFLT_MIN_LOOP_SZE  = 2
DFLT_MAX_LOOP_SZE  = 30


#########################################################################
#########################################################################
#########################################################################
def mk_dir(name_dir):
    if not(os.path.exists(name_dir)):
        os.mkdir(name_dir)
    name_dir += '/'
    return name_dir

## Remove homologs
def remove_homolog(path_to_pdb_clusters, targetAddr, model_dir, pdbID):
    """
    Identify homologs of protName in PDB, based on name and pdbclusters70
    """
    PDBcode = []
    flag = 0
    clusterID = open(model_dir + "homologs.list","w")
    if pdbID is not None:
        line = open(path_to_pdb_clusters + "bc-70.out","r").readlines()
        for ll in range(0,len(line)):
            cluster = line[ll].split(" ")[0:-1]
            pdbClust = []
            for k in range(0,len(cluster)):
                pdbClust.append(str.lower(cluster[k]).split("_")[0])
            if(pdbID in pdbClust):
                flag = 1
                break
        if flag == 0: return PDBcode
        for i in range(0,len(pdbClust)):
            code = "pdb" + pdbClust[i]
            if code not in PDBcode:
                PDBcode.append(code)

        for i in range(0,len(PDBcode)):
            clusterID.write(PDBcode[i] + "\n")
    return

def find_gap(GappedSeq, NativeSeq,modeling_mode):

    gap_start = []; gap_end = []; before_gap_start_ind = []; Loop_seq = []
    len_seq = len(NativeSeq)
    ind_nat = 0; no_gap = 0
    while ind_nat < len_seq:
        this_seq_pos = GappedSeq[ind_nat]
        if this_seq_pos == '-':
            Rstart = ind_nat
            while ind_nat < len_seq and GappedSeq[ind_nat] == '-' :
                ind_nat += 1
            looplength = ind_nat - Rstart
            gap_start.append(Rstart + 1)
            gap_end.append(ind_nat )
            Loop_seq.append(NativeSeq[Rstart : ind_nat])
            if modeling_mode == "Modeling":
                before_gap_start_ind.append(no_gap - 1)
            else:
                before_gap_start_ind.append(Rstart - 1)
        ind_nat += 1
        no_gap += 1

    return gap_start, gap_end, before_gap_start_ind, Loop_seq

def parsing_seq(Model, Model_seq, seq1):
    '''
    parsing the gapped sequence and initial model (pdb) to find the loop positions
    any lower case or special character on the sequence is regarded as gap
    checking if there is any inconsistency between the sequence and pdb
    '''
    AA1 = "ACDEFGHIKLMNPQRSTVWY"
    seq2 = ''
    last_ind = int(Model[len(Model)-1].rNum())
    if last_ind > len(seq1):
        sys.stderr.write("There is a mismatch between the sequence and the structure!\n The sequence does not contain all the residues available on the structure!\n Or the numbering does not start from 1!\n")
    native_len = max(last_ind, len(seq1))
    prot_seq = np.chararray(native_len)
    for i in range(native_len): prot_seq[i] = '-'
    for i in range(len(Model)):
        ind = int(Model[i].rNum()) - 1
        prot_seq[ind] = Model_seq[i]

    for i in range(len(seq1)):
        if seq1[i] in AA1:
            if seq1[i] != prot_seq[i]:
                sys.stderr.write("There is a mismatch between the sequence and the structure provided at position %d!\n" %(i+1))
            seq2 += seq1[i]
        else:
            seq2 += '-'
    return seq2

def parsing_str(Model, Model_seq, NativeSeq):
    '''
    parsing the gapped pdb and full sequence to find the loop positions
    checking if there is any inconsistency between the sequence and pdb
    '''
    first_ind = int(Model[0].rNum())
    last_ind = int(Model[len(Model)-1].rNum())
    if last_ind > len(NativeSeq):
        sys.stderr.write("There is a mismatch between the sequence and the structure!\n The sequence does not contain all the residues available on the structure!\n Or the numbering does not start from 1!\n")
    native_len = max(last_ind, len(NativeSeq))
    seq = np.chararray(native_len)
    for i in range(native_len): seq[i] = '-'
    for i in range(len(Model)):
        ind = int(Model[i].rNum()) - 1
        seq[ind] = Model_seq[i]
    seq = "".join(seq)
    return seq

def check_renumber(NativeSeq, gappedSeq, Model):
    '''
    checking the C- and N-terminals, cannot be modeled by DaReUS-Loop
    in case of terminal missing residues, the sequence and pdb are shrinked to the 
    first set of consecutive 4 residues at each side with both structure and sequence
    '''
    Nter = 0; Cter = len(gappedSeq)-1
    while gappedSeq[Nter] == '-':
        Nter += 1
    while gappedSeq[Cter] == '-':
        Cter -= 1

    gaped_seq = gappedSeq    #gappedSeq[Nter : (Cter+1)]
    ungaped_seq = NativeSeq  #NativeSeq[Nter : (Cter+1)]

    gapped_str = Model
    start_num = int(gapped_str[0].rNum())
    '''
    if start_num != 1:
        for i in range(len(gapped_str)):
            ind_ini = int(Model[i].rNum())
            gapped_str[i].rNum(str(ind_ini - start_num + 1))
    '''
    return ungaped_seq, gaped_seq, gapped_str

def modify_in_pdb(model_dir, input_PDB):
    '''
    check input pdb and prepare it for cyclization:
    read the first model, first chain, ignore water molecules and 
    convert MSE, CSE, HSE amino acids to MET, CYS and HIS
    take care of alternate coordinates
    '''
    ### check if the PDB file is valid
    if len(input_PDB) == 0:
        sys.stderr.write("Error! The input PDB file is not valid!")
        sys.exit(0)

    if len(input_PDB) < 8:
        sys.stderr.write("Error! The input PDB file is too small, there must be at least 8 residues present in the PDB!")
        sys.exit(0)

    ### read the first model and the first chain
    nb_models = input_PDB.nModels()
    if nb_models > 1:
        sys.stderr.write("Warning! Only the first model from the input PDB is considered!")
        input_PDB.setModel(1)

    all_chains = input_PDB.chnList()
    if len(all_chains) > 1:
        sys.stderr.write("Warning! Only the first chain from the input PDB is considered!")
        GappedPDB = input_PDB.nChn(all_chains[0])
    else:
        GappedPDB = input_PDB

    ### check for modified amino aids
    refe_seq = GappedPDB.aaseq()
    if 'X' in refe_seq:
        sys.stderr.write("Error! Please only provide standard amino acids in your PDB file!")
        sys.exit(0)

    ### clean the input PDB
    GappedPDB.clean()

    ### check for insertion codes
    mask_list2 = []
    for ll in range(len(GappedPDB)):
        if GappedPDB[ll].riCode() != ' ':
           mask_list2.append(GappedPDB[ll].rNum())
    if len(mask_list2) > 0:
        sys.stderr.write("Error! Please decide for the insertion codes: %s" %mask_list2)
        sys.exit(0)

    ### check for alternate atoms
    GappedPDB.out(model_dir + "modified_input_protein.pdb", altCare = 1)
    GappedPDB = PDB.PDB(model_dir + "modified_input_protein.pdb")
    nb_BB, list_BB = GappedPDB.BBatmMiss()
    if nb_BB > 0:
        sys.stderr.write("Error! Backbone atoms are missing in your input PDB: %s" %list_BB)
        sys.exit(0)
    return GappedPDB

## prepare input files
def prep_files(targetAddr, model_dir, protName, protSeq, flank_size, FST_seq, modeling_mode, out_label, mode, def_min_loop_size, def_max_loop_size):

    ### read the first model, first chain, ignore water molecules and convert MSE, CSE, HSE amino acids to MET, CYS and HIS
    if mode == "cluster":
        ModelPDB = PDB.PDB(model_dir + "modified_input_protein.pdb")
    else:
        input_PDB = PDB.PDB(targetAddr + protName)
        ModelPDB = modify_in_pdb(model_dir, input_PDB)
    
    ### find loop positions
    if modeling_mode == "Modeling":
        Model_seq = ModelPDB.aaseq()
        NativeSeq = protSeq
        gappedSeq = parsing_str(ModelPDB, Model_seq, protSeq)
    else:
        NativeSeq = ModelPDB.aaseq()
        gappedSeq = parsing_seq(ModelPDB, NativeSeq, protSeq)

    ### fst input for PyPPP ###################################
    if FST_seq is None:
       FST_seq = NativeSeq
    fst_file = open(model_dir + "target_seq.fst", "w")
    fst_file.write(">%s\n%s\n" %(protName, FST_seq))

    ### verifying the input sequence and structure ############
    if len(NativeSeq) != len(gappedSeq):
        sys.stderr.write("There is a mismatch between the sequence and the structure you provided! Please make sure of the input data!\n")
        sys.exit(0)
    ### check the terminals and renumber the pdb if needed ####
    ungaped_seq, gaped_seq, gapped_str = check_renumber(NativeSeq, gappedSeq, ModelPDB) 
    gapped_str.out(model_dir + "Gapped_renumbered.pdb")
    gapped_str = PDB.PDB(model_dir + "Gapped_renumbered.pdb")
    GapedLen   = len(gaped_seq)
    UnGapedLen = len(ungaped_seq)
    ##### identifying all the loops
    loop_info = open(targetAddr + out_label + "_loops.log","w")
    loop_info.write("#number sequence size start res_ind_before_gap\n")
    Gap_Start, Gap_End, before_gap, LoopSeq = find_gap(gaped_seq, ungaped_seq, modeling_mode)
    nbLoop = len(Gap_Start)
    ind = 0
    for j in range(0,nbLoop):
        GapStart = Gap_Start[j]
        looplength = len(LoopSeq[j])
        if GapStart == 1:  
           sys.stderr.write("Warning! Ignoring the N-terminal loop of size %d.\n" %(looplength))
           continue
        if (GapStart+looplength-1) >= UnGapedLen:
           sys.stderr.write("Warning! Ignoring the C-terminal loop of size %d.\n" %(looplength))
           continue
        ## ignoring gaps of size 2
        if looplength < def_min_loop_size:
           sys.stderr.write("Warning! Minimum loop size is %d. Ignoring one loop of size %d.\n" % (def_min_loop_size, looplength) )
           continue
        ## ignoring to model long loops with more than 30 residues
        if looplength > def_max_loop_size:
           sys.stderr.write("Warning! Maximum loop size is %d. Ignoring one loop of size %d.\n" % (def_max_loop_size, looplength) )   
           continue
        ## saving the loop details
        loop_info.write("%d %s %d %d %d\n" %(ind+1, LoopSeq[j], len(LoopSeq[j]), GapStart, before_gap[j]))
        Loop_dir = mk_dir(model_dir + "Loop" + str(ind+1) + "_" + LoopSeq[j])
        Loop_BC_dir = mk_dir(Loop_dir + "BCSearch")
        ind += 1
        
        # Preparing the flanks
        if modeling_mode == "Modeling":
           after_gap_ind = before_gap[j] + 1
	else:
	   after_gap_ind = before_gap[j] + looplength + 1 
        flank1 = gapped_str[(before_gap[j] + 1 - flank_size): (before_gap[j] + 1)]    
        flank2 = gapped_str[after_gap_ind : (after_gap_ind + flank_size)]
        flank1.out(Loop_dir + "flank1.pdb")
        flank2.out(Loop_dir + "flank2.pdb")

        ##### if ungageped model is provided, we use it to detect clashes, otherwise the input gaped PDB is used
        if modeling_mode == "Modeling":
           GappedPDB = gapped_str
        else:
           GappedPDB = gapped_str[0:(GapStart-1)] + gapped_str[Gap_End[j]:UnGapedLen]
        GappedPDB.out(Loop_dir + "GappedPDB.pdb")
        
    return
    
def print_options(options):
    """
    @return: a string describing the options setup for the run
    """
    try:
        rs = ""
        for option in options.__dict__.keys():
            rs = "%s\n  %10s: %s" % (rs, str(option),getattr(options,option))
        return rs
    except:
        return ""


# -- ARGUMENTS --
def cmdLine():
    """
    Command line definition.
    @return: command line options and arguments (optionParser object)
    """
    parser = argparse.ArgumentParser()

    parser.add_argument("--flank_sze", dest="flank_sze", type = int, help="Flank size (%d)" % DFLT_FLANK_SZE,
                        action="store", default=DFLT_FLANK_SZE)
    
    parser.add_argument("--wpath", dest="work_path", help="path to target (%s)" % DFLT_WORK_PATH,
                        action="store", default=DFLT_WORK_PATH)

    parser.add_argument("--mode", dest="mode", help="Modeling or Re-Modeling",
                        action="store", required=True, default = None)

    parser.add_argument("--model_str", dest="model_str", help="Modeling: gaped structure, Remodeling: initial model",
                        action="store", required=True, default=None)

    parser.add_argument("--model_seq", dest="model_seq", help="Modeling: full structure, Remodeling: gaped sequence",
                        action="store", required=True, default=None)

    parser.add_argument("--fst_seq", dest="fst_seq", help="un-gapped full sequence to calculate the local conformation profile",
                        action="store", default=None)

    parser.add_argument("--pdb_id", dest="pdb_id", help="PDB to remove the homologs",
                        action="store", default = None)
    
    parser.add_argument("--cls_path", dest="pdb_cls_path", help="path to PDB clusters (%s)" % DFLT_PDB_CLS_PATH,
                        action="store", default=DFLT_PDB_CLS_PATH)

    parser.add_argument("--min_loop_sze", dest="min_loop_sze", type = int, help="Minimal loop size",
                        action="store", default=DFLT_MIN_LOOP_SZE)

    parser.add_argument("--max_loop_sze", dest="max_loop_sze", type = int, help="Maximal loop size",
                        action="store", default=DFLT_MAX_LOOP_SZE)
    
    parser.add_argument("--l", dest="Lbl", help="the label for the output results.",
                        action="store", default=DFLT_LBL)

    parser.add_argument("--run_mod", dest="run_mod", help="running mode (\"cluster\" or \"local\")(%s)" % DFLT_MODE,
                        action="store", default=DFLT_MODE)

    return parser


def argParse():
    """
    Check and parse arguments.
    """
    parser = cmdLine()
    try:
        options = parser.parse_args()
    except ValueError:
        parser.error("\nInvalid command line format !\n")

    if options.work_path[-1] != "/":
        options.work_path = options.work_path + "/" 
    print print_options(options)           
    return options


if __name__=="__main__":
    """
    Parse the command line and launch the main purpose.
    """
    options = argParse()

    targetAddr           = options.work_path
    protName             = options.model_str
    protSeq              = options.model_seq
    modeling_mode        = options.mode
    flank_size           = options.flank_sze
    PyPPP_seq            = options.fst_seq
    pdbID                = options.pdb_id
    path_to_pdb_clusters = options.pdb_cls_path
    out_label            = options.Lbl
    mode                 = options.run_mod
    def_min_loop_size    = options.min_loop_sze
    def_max_loop_size    = options.max_loop_sze
    
    # files
    model_dir = targetAddr + PROT_DIR + '/'
    remove_homolog(path_to_pdb_clusters, targetAddr, model_dir, pdbID)
    prep_files(targetAddr, model_dir, protName, protSeq, flank_size, PyPPP_seq, modeling_mode, out_label, mode, def_min_loop_size, def_max_loop_size)

