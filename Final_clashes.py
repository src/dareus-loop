#!/usr/bin/env python2.7

"""
author: Yasaman Karami
5 July 2018
MTi
"""
###########################
###  L I B R A R I E S  ###
###########################
import sys
import numpy as np
from PyPDB import PyPDB as PDB
import sys, os
import argparse
from Bio import pairwise2
from rmsd import *
from scipy.spatial import cKDTree as KDTree

DFLT_WORK_PATH     = "./"
PROT_DIR           = "DaReUS_Loop"
DFLT_FLANK_SZE     = 4
DFLT_MODE          = "local"  #else "cluster"
DFLT_LBL           = "dareus"
DFLT_ADV           = 0
DFLT_CLASH_TH      = 3
#########################################################################
#########################################################################
def read_loops_info(targetAddr, Loop_info):
    nbLoop = len(Loop_info) - 1
    Loops = np.zeros((nbLoop,4))
    seq=[]; ind = 0; Loop_name=[]
    for i in range(len(Loop_info)):
       if Loop_info[i].startswith("#"): continue
       CandidFileLine = Loop_info[i].split()
       Loops[ind,0] = int(CandidFileLine[3]) - 1  #Loop start
       Loops[ind,1] = int(CandidFileLine[2]) + Loops[ind,0]  #Loop stop
       Loops[ind,2] = int(CandidFileLine[4]) + 1 # before Loop start
       Loops[ind,3] = int(CandidFileLine[2])  #Loop size
       ind += 1
       seq.append(CandidFileLine[1]) 
       Loop_name.append("Loop" + CandidFileLine[0] + "_" + CandidFileLine[1])
    return Loops[0:ind,], seq, Loop_name

def find_clash(loop_bb, clash_body_tree, Clash_th):
    FinalClashes = 0
    if clash_body_tree is not None:
        clashes = clash_body_tree.query_ball_point(loop_bb,Clash_th)
        for l in clashes:
          for x in l:
            FinalClashes = FinalClashes + 1
    return FinalClashes

def write_clash(targetAddr, model_dir, out_label):
    '''
    clash verification
    for all the top candidates of loop #nb_line, we consider all the combinations
    with other loops candidates and write down all the results
    '''
    ### all the Loops
    Loop_info = open(targetAddr + out_label + "_loops.log","r").readlines()
    Loops_id, seq, Loops_name = read_loops_info(targetAddr, Loop_info)
    nbLoops = len(seq)
    nbCandid = np.zeros(nbLoops, dtype=int)
    all_loops = {}
    for i in range(nbLoops):
        all_loops[Loops_name[i]] = {}
        score_file = targetAddr + out_label + "_" + Loops_name[i] + "_summary.txt" 
        if not os.path.isfile(score_file) or os.path.getsize(score_file) <= 0 : continue
        RMSDfile = open(score_file,"r").readlines()
        nbb = 0
        for j in range(len(RMSDfile)):
            if RMSDfile[j].startswith("#"): continue 
            candid = RMSDfile[j].split()
            candid_pdb = PDB.PDB(model_dir + candid[0])
            loop_seg = candid_pdb[int(Loops_id[i,2]) : (int(Loops_id[i,2]) + int(Loops_id[i,3]))]
            LoopSeg_bb = get_bb(loop_seg)
            all_loops[Loops_name[i]][nbb] = LoopSeg_bb
            nbb += 1
        nbCandid[i] = nbb

    clash_file = open(targetAddr + out_label + "_clashes.list", "w")
    for i in range(0,(nbLoops-1)):
        Loop_name1 = Loops_name[i]
        for C1 in range(0,nbCandid[i]):
            this_loop_bb = all_loops[Loop_name1][C1]
            this_loop_tree = KDTree(this_loop_bb)
            for j in range((i+1),nbLoops):
                Loop_name2 = Loops_name[j]
                for C2 in range(0,nbCandid[j]):
                    other_loop_bb = all_loops[Loop_name2][C2]
                    clash_val = find_clash(other_loop_bb, this_loop_tree, DFLT_CLASH_TH)
                    if clash_val > 0:
                        name1 = out_label + "_" + Loop_name1 + "_model" + str(C1+1)
                        name2 = out_label + "_" + Loop_name2 + "_model" + str(C2+1)
                        clash_file.write("%s %s\n" %(name1, name2))
    return

def print_options(options):
    """
    @return: a string describing the options setup for the run
    """
    try:
        rs = ""
        for option in options.__dict__.keys():
            rs = "%s\n  %10s: %s" % (rs, str(option),getattr(options,option))
        return rs
    except:
        return ""


# -- ARGUMENTS --
def cmdLine():
    """
    Command line definition.

    @return: command line options and arguments (optionParser object)
    """

    parser = argparse.ArgumentParser()
    
    parser.add_argument("--wpath", dest="work_path", help="path to target (%s)" % DFLT_WORK_PATH,
                        action="store", default=DFLT_WORK_PATH)

    parser.add_argument("--l", dest="Lbl", help="the label for the output results.",
                        action="store", default=DFLT_LBL)

    
    return parser


def argParse():
    """
    Check and parse arguments.
    """

    parser = cmdLine()

    try:
        options = parser.parse_args()
    except ValueError:
        parser.error("\nInvalid command line format !\n")

    if options.work_path[-1] != "/":
        options.work_path = options.work_path + "/"
        
    print print_options(options)    
        
    return options


if __name__=="__main__":
    """
    Parse the command line and launch the main purpose.
    """

    options = argParse()

    targetAddr  = options.work_path
    out_label   = options.Lbl

    model_dir = targetAddr + PROT_DIR + "/"
    write_clash(targetAddr, model_dir, out_label)

