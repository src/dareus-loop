#!/usr/bin/env python2.7

"""
Scripts taken from:
BCLoopSearch wrapper
Sjoerd de Vries, MTi, 2016
"""
###########################
###  L I B R A R I E S  ###
###########################
import string
import glob
import mmap
import PyPDB.PyPDB as PDB
from PyPDB import *
from PyPDB.Geo3DUtils import *
import numpy as np
from scipy.spatial.distance import squareform

"""
Scripts taken from:
BCLoopSearch wrapper
Sjoerd de Vries, MTi, 2016
"""
def get_ca(pdb):
    coors = []
    for res in pdb:
        resnr = int(res[0].resNum())
        for atom in res:
            if atom.alt() not in (" ", "A"): continue
            if atom.atmName().strip() == "CA":
                coors.append(atom.xyz())
    return np.array(coors, dtype = "double")

def get_bb(pdb):
    BB = ["CA","C","O","N"]
    coors = []
    for res in pdb:
        resnr = int(res[0].resNum())
        for atom in res:
            if atom.alt() not in (" ", "A"): continue
            if atom.atmName().strip() in BB:
                coors.append(atom.xyz())
    return np.array(coors, dtype = "double")

def get_all(pdb):
    coors = []
    for res in pdb:
        resnr = int(res[0].resNum())
        for atom in res:
            if atom.alt() not in (" ", "A"): continue            
            coors.append(atom.xyz())
    return np.array(coors, dtype = "double")

def update_bb(pdb, coors):
    ind = 0
    for res in pdb:
        resnr = int(res[0].resNum())
        for atom in res:
            if atom.alt() not in (" ", "A"): continue
            namee = atom.atmName().strip()
            if namee == "CA" or namee == "C" or namee == "N" or namee == "O":
                atom.setcrds(coors[ind][0], coors[ind][1], coors[ind][2])
                ind = ind + 1
    return pdb

def update_all(pdb, coors):
    ind = 0
    for res in pdb:
        resnr = int(res[0].resNum())
        for atom in res:
            if atom.alt() not in (" ", "A"): continue
            namee = atom.atmName().strip()
            atom.setcrds(coors[ind][0], coors[ind][1], coors[ind][2])
            ind = ind + 1
    return pdb

def rmsd(atoms1, atoms2):
  assert len(atoms1) == len(atoms2)
  assert len(atoms1) > 0
  d = atoms1 - atoms2
  d2 = d * d
  sd = d2.sum(axis=1)
  return np.sqrt(sd.mean())

def apply_matrix(atoms, pivot, rotmat, trans):
  ret = []
  for atom in atoms:
    a = atom-pivot
    atom2 = a.dot(rotmat) + pivot + trans
    ret.append(atom2)
  return ret


def fit(atoms1, atoms2):
  # adapted from QKabsch.py by Jason Vertrees.
  # further adapted from irmsd for fitting by Sjoerd de Vries
  assert len(atoms1) == len(atoms2)
  assert len(atoms1) > 0
  L = len(atoms1)

  # must alway center the two proteins to avoid
  # affine transformations.  Center the two proteins
  # to their selections.
  COM1 = np.sum(atoms1,axis=0) / float(L)
  COM2 = np.sum(atoms2,axis=0) / float(L)
  atoms1 = atoms1 - COM1
  atoms2 = atoms2 - COM2

  # Initial residual, see Kabsch.
  E0 = np.sum( np.sum(atoms1 * atoms1,axis=0),axis=0) + np.sum( np.sum(atoms2 * atoms2,axis=0),axis=0)

  #
  # This beautiful step provides the answer.  V and Wt are the orthonormal
  # bases that when multiplied by each other give us the rotation matrix, U.
  # S, (Sigma, from SVD) provides us with the error!  Isn't SVD great!
  V, S, Wt = np.linalg.svd( np.dot( np.transpose(atoms1), atoms2))

  # We already have our solution, in the results from SVD.
  # we just need to check for reflections and then produce
  # the rotation.  V and Wt are orthonormal, so their det's
  # are +/-1.0 (and thus products are +/- 1.0 ).
  reflect = float(str(float(np.linalg.det(V) * np.linalg.det(Wt))))

  if reflect == -1.0:
          S[-1] = -S[-1]
          V[:,-1] = -V[:,-1]


  U = V.dot(Wt).transpose()
  RMSD = E0 - (2.0 * sum(S))
  RMSD = np.sqrt(abs(RMSD / L))
  return U, COM1-COM2, RMSD
  

def all_rmsd_chunk(chunk1, chunk2):
    natoms = chunk1.shape[1]
    delta = chunk1[:,None,:,:] - chunk2[None,:,:,:]
    dsq = np.sum(delta**2, axis=-1)
    chunk_allrmsd = np.sqrt(np.sum(dsq,axis=2)/natoms)
    return chunk_allrmsd

def RMSDall(allca,flankLen,loopLen):
    #pdbfiles = [l.strip() for l in open(listfile).readlines() if len(l.strip())]
    '''
    pdb = PDB.PDB(pdbfiles[0])[flankLen:(flankLen + loopLen)]
    ca = get_ca(pdb)
    allca = np.zeros((len(pdbfiles), len(ca), 3))
    for pdbfileno, pdbfile in enumerate(pdbfiles):
        pdb = PDB.PDB(pdbfile)[flankLen:(flankLen + loopLen)]
        ca = get_ca(pdb)
        assert len(ca) == allca.shape[1], (len(ca), allca.shape[1])
        allca[pdbfileno] = ca
    '''
    natoms = allca.shape[1]
    nstruc = len(allca)
    chunksize = 500

    allrmsd  = np.zeros((nstruc, nstruc))
    nchunks = int(float(nstruc)/chunksize+0.9999999)
    for i in range(nchunks):
        start1 = i*chunksize
        end1 = (i+1) * chunksize
        c1 = allca[start1:end1]
        for j in range(i,nchunks):
            start2 = j*chunksize
            end2 = (j+1) * chunksize
            c2 = allca[start2:end2]
            chunk_allrmsd = all_rmsd_chunk(c1, c2)
            allrmsd[start1:end1,start2:end2] = chunk_allrmsd
            allrmsd[start2:end2,start1:end1] = chunk_allrmsd.transpose()
    return allrmsd

def hit_pdb(bb, resnrs, seq):
    AA1 = "ACDEFGHIKLMNPQRSTVWY"
    AA1seq = "ACDEFGHIKLMNPQRSTVWYXXXSXWMCXWYMDPECXXYAMMSCMAHHHZB"
    AA3 = ["ALA","CYS","ASP","GLU","PHE","GLY","HIS","ILE","LYS","LEU","MET","ASN","PRO","GLN","ARG","SER","THR","VAL","TRP","TYR","5HP","ABA","PCA","FGL","BHD","HTR","MSE","CEA","ALS","TRO","TPQ","MHO","IAS","HYP","CGU","CSE","RON","3GA","TYS", "AYA", "FME", "CXM", "SAC", "CSO", "MME", "SEG", "HSE", "HSD","HSP","GLX","ASX"]
    AA3STRICT = ["ALA","CYS","ASP","GLU","PHE","GLY","HIS","ILE","LYS","LEU","MET","ASN","PRO","GLN","ARG","SER","THR","VAL","TRP","TYR"]
    txt = ""
    tmpl = "ATOM   %4d  %s  %s  %4d    %8.3f%8.3f%8.3f  1.00  0.00\n"
    atomnames = "N ", "CA", "C ", "O "
    bb = bb.reshape(len(bb)*4, 3)
    for atomnr, atom in enumerate(bb):
        resindex = atomnr//4
        resnr = resnrs[resindex]
        if atom[0] != -999 or atom[1] != -999 or atom[2] != -999:
            AA_single = seq[resindex]
            AA1_ind = AA1seq.index(AA_single)
            AA_Triple = AA3[AA1_ind]
            txt += tmpl % (atomnr+1, atomnames[atomnr%4], AA_Triple, resnr, atom[0], atom[1],atom[2])
    return txt