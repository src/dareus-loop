#!/usr/bin/env python

from setuptools import setup
import glob

setup(name='dareus-loop',
      description='RPBS services for python 2.7',
      author='Julien Rey',
      author_email='julien.rey@univ-paris-diderot.fr',
      url='bioserv.rpbs.univ-paris-diderot.fr',
      packages=['BC'],
      package_data={'BC': ['lib/*','src/*']},
      scripts=glob.glob('*.py'),
)
