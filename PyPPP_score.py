#!/usr/bin/env python2.7

"""
author: Yasaman Karami
5 July 2018
MTi
"""
###########################
###  L I B R A R I E S  ###
###########################
import sys
import os
from PyPDB import PyPDB as PDB
from PyPDB.Geo3DUtils import *
import numpy as np
from optparse import OptionParser
from Bio import SeqIO
import multiprocessing as mp
import argparse

DFLT_NPROC         = 8
DFLT_WORK_PATH     = "./"
DFLT_PDB_NAME      = "pdbChainID"
DFLT_FLANK_SZE     = 4
DFLT_LINE_NB       = 0
pseudo_count       = 0.000001
DFLT_MODE          = "local"
PROT_DIR           = "DaReUS_Loop"
DFLT_BANK          = "/scratch/banks/bc_banks"
DFLT_PROF_PATH     = DFLT_BANK + "/PyPPP_profiles/"
MAX_JOB            = 300
######################################
#### Jenson Shanon ###################
def ShenEnt(vec):
    s1 = 0
    for i in range(0,27):
      log2val = np.log2(vec[i])
      s1 += (vec[i] * log2val)
    return (s1 * -1)

def JenShen(p1,p2):
    term1 = 0; term2 = 0; term3 = 0
    term1vec = np.zeros(27)

    indd = np.argwhere(p1==0)
    if len(indd) > 0: p1[indd] = pseudo_count
    indd = np.argwhere(p2==0)
    if len(indd) > 0: p2[indd] = pseudo_count

    for j in range(0,27):
        term1vec[j] = (p1[j] + p2[j])/2
    term1 = ShenEnt(term1vec)

    H_exp = ShenEnt(p1)
    term2 = H_exp
    H_pred = ShenEnt(p2)
    term3 = H_pred

    score = term1 - (0.5 * term2) - (0.5 * term3)
    return score

def JenShenAll(p1,p2,lenSeq):
    JSD = 0; JCD_max = 0
    for i in range(0,lenSeq):
      val = JenShen(p1[i,],p2[i,])
      JSD += val
      if val > JCD_max:
        JCD_max = val
    JSD = JSD / lenSeq
    return JSD, JCD_max

def read_file(file_to_read, size2):

    if not os.path.exists(file_to_read):
        hit_prf = np.zeros((1,size2))
        return hit_prf
    file_lines = open(file_to_read,'r').readlines()
    lenFile = len(file_lines)
    hit_prf = np.zeros((lenFile,size2))
    for i in range(0,lenFile):
        linee = file_lines[i].split("\n")[0]
        hit_file_line = linee.split(" ")
        for j in range(0,size2):
            hit_prf[i,j] = float(hit_file_line[j])
    return hit_prf

def read_seq(hit_name,refe_prf_start,candid_name,candid_prf_start,looplength):

    hit_prf = read_file(hit_name, 27)
    candid_prf = read_file(candid_name, 27)
    if (candid_prf_start+looplength) > len(candid_prf) : #when PyPPP is not applied
        return "C_err", "C_err"
    if (refe_prf_start+looplength) > len(hit_prf) :
        return "R_err", "R_err"
    sub_hit_prf = hit_prf[refe_prf_start : (refe_prf_start+looplength),]
    sub_candid_prf = candid_prf[candid_prf_start : (candid_prf_start+looplength),]
    score = JenShenAll(sub_hit_prf,sub_candid_prf,looplength)
    return score


def JSD(CandidFile, target_name, LoopSize, refe_prf_start, Waddr, flank_size, PyPPP_addr, hit_name):

    candid = CandidFile.split()
    pdb_code = candid[0]
    pdb_chain = candid[1]
    pdb_model = int(candid[2])
    pdb_start = int(candid[3])
    pdb_end = int(candid[4])
    CandidSeq = candid[11]

    candid_start = pdb_start + flank_size
    name = pdb_code + "-" + pdb_chain

    fst_name = PyPPP_addr + name + ".fst"
    if not os.path.exists(fst_name):
       sys.stderr.write("The fst file for " + fst_name + "does not exist!\n")
       return
    candid_seq_file = list(SeqIO.parse(fst_name, "fasta"))
    candid_seq = candid_seq_file[0].seq
    candid_prf_start = candid_seq.find(CandidSeq)
    if candid_prf_start == -1:
        sys.stderr.write("There is a break in the selected loop candidate!\n")
        return
    ## Score
    candid_name = PyPPP_addr + name + ".svmi8.27.prob"
    if LoopSize > 3:
       ppp_read_size = LoopSize - 3
    else:
       ppp_read_size = 1
    PyPPP_score1, PyPPP_score2 = read_seq(hit_name, refe_prf_start, candid_name, candid_prf_start, ppp_read_size)
    hit = CandidFile.split("\n")[0] + " " + str(PyPPP_score1) + " " + str(PyPPP_score2)
    ### write
    pattern = pdb_code + "-" + pdb_chain + "-" + str(pdb_model) + "-" + str(pdb_start) + "-" + str(pdb_end)
    if PyPPP_score1 <= 0.4:
       ScoreFile = open(Waddr + "PyPPP_score_" + pattern + ".txt", "w")
       ScoreFile.write(hit + "\n")
    else:
        sys.stderr.write("%s: PyPPP score lower than 0.4 => rejected" % pattern)

    return

def JSD_thread(CandidFile,target,LoopSize,refe_prf_start,Loop_dir,flank_size,PyPPP_dir,target_prof):
    nbHit = len(CandidFile)
    nchunks = int(float(nbHit)/nbThreads+0.9999999)
    for j in range(nchunks):
        start1 = j*nbThreads
        end1 = (j+1) * nbThreads
        if end1 >= nbHit:
            nbCPU = nbHit - start1
        else:
            nbCPU = nbThreads

        threads = []
        for k in range(nbCPU):
            p = mp.Process(target=JSD, args=(CandidFile[start1 + k], target,LoopSize,refe_prf_start,Loop_dir,flank_size,PyPPP_dir,target_prof,))
            p.start()
            threads.append(p)
        for t in threads:
            t.join()
    return

def print_options(options):
    """
    @return: a string describing the options setup for the run
    """
    try:
        rs = ""
        for option in options.__dict__.keys():
            rs = "%s\n  %10s: %s" % (rs, str(option),getattr(options,option))
        return rs
    except:
        return ""


if __name__=="__main__":


    parser = argparse.ArgumentParser()

    parser.add_argument("--ppp_path", dest="ppp_path", help="path to profiles(%s)" % DFLT_PROF_PATH,
                        action="store", default=DFLT_PROF_PATH)

    parser.add_argument("--loop_path", dest="loop_path", help="loop path",
                        action="store", default=None)

    parser.add_argument("--target", dest="target", help="gapped PDB to model the missing region",
                        action="store", required=True, default = None)

    parser.add_argument("--wpath", dest="work_path", help="path to target (%s)" % DFLT_WORK_PATH,
                        action="store", default=DFLT_WORK_PATH)

    parser.add_argument("--loop_seq", dest="loop_seq", help="the id of the first residue on the loop to be modeled",
                        action="store", default=None)

    parser.add_argument("--flank_sze", dest="flank_sze", type = int, help="Flank size (%d)" % DFLT_FLANK_SZE,
                        action="store", default=DFLT_FLANK_SZE)

    parser.add_argument("--run_mod", dest="run_mod", help="running mode (\"cluster\" or \"local\")(%s)" % DFLT_MODE,
                        action="store", default=DFLT_MODE)

    parser.add_argument("--np", dest="nproc", type = int, help="number of processors (%d)" % DFLT_NPROC,
                        action="store", default=DFLT_NPROC)


    options = parser.parse_args() # Do not pass sys.argv
    # print options.__dict__.keys()
    if options.work_path[-1] != "/":
        options.work_path = options.work_path + "/"
    print print_options(options)

    #    sys.exit(0)

    # input
    PyPPP_dir  = options.ppp_path
    targetAddr = options.work_path
    LoopSeq    = options.loop_seq
    target     = options.target
    flank_size = options.flank_sze
    mode       = options.run_mod
    nbThreads  = options.nproc
    Loop_dir  = options.loop_path

    LoopSize = len(LoopSeq)
    model_dir = targetAddr + PROT_DIR + "/"
    BC_dir = Loop_dir + "BCSearch/"

    PPP_name = "target_seq"
    fst_name = model_dir + PPP_name + ".fst"
    if not os.path.isfile(fst_name):
       sys.stderr.write("Conformation profile does not exist!\n")
    target_prof = model_dir + PPP_name + ".svmi8.27.prob"
    target_seq_file = list(SeqIO.parse(fst_name, "fasta"))
    target_seq = target_seq_file[0].seq
    refe_prf_start = target_seq.find(LoopSeq)
    CandidFile = open(BC_dir + "BCLoopCluster_Final_BC.txt", "r").readlines()
    nbCandid = len(CandidFile)

    if mode == "cluster":
        nb_line = int(os.environ['TASK_ID']) - 1

        nb_repeat = int(float(nbCandid)/MAX_JOB+0.9999999)
        for j in range(0,nb_repeat):
            ind_line = (j * MAX_JOB) + nb_line
            if ind_line >= nbCandid: continue
            CandidFileLine = CandidFile[ind_line]
            if CandidFileLine.startswith("#"):
                sys.stderr.write("Error reading BCLoopSearch results file\n")
                continue
            JSD(CandidFileLine,target,LoopSize,refe_prf_start,BC_dir,flank_size,PyPPP_dir,target_prof)

    else:
        JSD_thread(CandidFile,target,LoopSize,refe_prf_start,BC_dir,flank_size,PyPPP_dir,target_prof)
