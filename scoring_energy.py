#!/usr/bin/env python2.7

"""
author: Yasaman Karami
5 July 2018
MTi
"""
###########################
###  L I B R A R I E S  ###
###########################
import sys
import numpy as np
from PyPDB import PyPDB as PDB
import sys, os
import argparse

DFLT_WORK_PATH     = "./"
PROT_DIR           = "DaReUS_Loop"
DFLT_FLANK_SZE     = 4
DFLT_MODE          = "local"  #else "cluster"
DFLT_LBL           = "dareus"
DFLT_ADV           = 0
#########################################################################
#########################################################################
def run_Scoring(targetAddr, out_label, nb_line, advanced_run, modeling_mode):
    '''
    selecting and writing the final top 10 candidates for loop #nb_line
    top 5 candidates based on JSD and
    top 5 candidates based on flank RMSD
    '''
    model_dir = targetAddr + PROT_DIR + "/"
    ### current loop
    Loop_info = open(targetAddr + out_label + "_loops.log","r").readlines()
    linee = Loop_info[nb_line].split()
    candid_dir = model_dir + "Candidates/"
    this_loop_name = "Loop%d_%s" %(int(linee[0]), linee[1])
    ##### writing final results
    score_table = open(targetAddr + out_label + "_" + this_loop_name + "_scores.txt", "w")
    score_table.write("#Name GROMACS_potential KORP_score\n")
    score_file = targetAddr + out_label + "_" + this_loop_name + "_summary.txt"
    if not os.path.isfile(score_file) or os.path.getsize(score_file) <= 0 :
        return
    ################ Scoring (Flank RMSD, JSD) ###########################################################
    ScoreLine = open(score_file,"r").readlines()
    nbCandid = len(ScoreLine) - 1
    if nbCandid == 0: return
    for j in range(nbCandid):
        linee = ScoreLine[j+1]
        candid = linee.split()
        name = candid[1] + "-" + candid[2] + "-" + candid[3] + "-" + candid[4] + "-" + candid[5]
        candid_file = candid_dir + this_loop_name + "_" + name + "_mini.pdb"
        candid_file_unref = candid_dir + this_loop_name + "_" + name + "_model.pdb"
        model_name = out_label + "_" + this_loop_name + "_model" + str(j+1) + ".pdb"
        if not os.path.exists(candid_file):
            if not os.path.exists(candid_file_unref): continue
            candid_file = candid_file_unref
        ################################################################
        GROMACS_energy = "NA"
        KORP_score = "NA"

        gro_file = candid_dir + "em/min_" + this_loop_name + "_" + name + "_mini.log"
        if os.path.exists(gro_file):
            os.system("grep \"Potential Energy  =\" " + gro_file + " > " + targetAddr + "ener_" + this_loop_name)
            gro_line = open(targetAddr + "ener_" + this_loop_name,"r").readlines()[0].split()
            GROMACS_energy = gro_line[-1]
            os.system("rm " + targetAddr + "ener_" + this_loop_name)

        korp_file = targetAddr + out_label + "_" + this_loop_name + "_models.pdb.korpescore"
        if os.path.exists(korp_file):
            krop_line = open(korp_file, "r").readlines()[j-1]
            KORP_score = krop_line.split()[0]

        score_table.write("%s %s %s\n" %(model_name, GROMACS_energy, KORP_score))
        ###
    score_table.close()
    return

def print_options(options):
    """
    @return: a string describing the options setup for the run
    """
    try:
        rs = ""
        for option in options.__dict__.keys():
            rs = "%s\n  %10s: %s" % (rs, str(option),getattr(options,option))
        return rs
    except:
        return ""


# -- ARGUMENTS --
def cmdLine():
    """
    Command line definition.

    @return: command line options and arguments (optionParser object)
    """

    parser = argparse.ArgumentParser()
    
    parser.add_argument("--wpath", dest="work_path", help="path to target (%s)" % DFLT_WORK_PATH,
                        action="store", default=DFLT_WORK_PATH)

    parser.add_argument("--run_mod", dest="run_mod", help="running mode (\"cluster\" or \"local\")(%s)" % DFLT_MODE,
                        action="store", default=DFLT_MODE)

    parser.add_argument("--l", dest="Lbl", help="the label for the output results.",
                        action="store", default=DFLT_LBL)

    parser.add_argument("--advanced", dest="advanced", type = int, help="Modeling loops independently!",
                        action="store", default=DFLT_ADV)

    parser.add_argument("--mode", dest="mode", help="Modeling or Re-Modeling",
                        action="store", default = None)


    
    return parser


def argParse():
    """
    Check and parse arguments.
    """

    parser = cmdLine()

    try:
        options = parser.parse_args()
    except ValueError:
        parser.error("\nInvalid command line format !\n")

    if options.work_path[-1] != "/":
        options.work_path = options.work_path + "/"
        
    print print_options(options)    
        
    return options


if __name__=="__main__":
    """
    Parse the command line and launch the main purpose.
    """

    options = argParse()

    targetAddr    = options.work_path
    mode          = options.run_mod
    out_label     = options.Lbl
    advanced_run  = options.advanced
    modeling_mode = options.mode

    Loop_info = open(targetAddr + out_label + "_loops.log","r").readlines()
    nbLoop = len(Loop_info) - 1
    if mode == "cluster":
        nb_line = int(os.environ['TASK_ID'])
        if Loop_info[nb_line].startswith("#"):
           sys.stderr.write("wrong loop info!\n")
        CandidFileLine = Loop_info[nb_line].split()
        Loop_name = "Loop" + CandidFileLine[0] + "_" + CandidFileLine[1]
        run_Scoring(targetAddr, out_label, nb_line, advanced_run, modeling_mode)
    else:
        for i in range(nbLoop):
           CandidFileLine = Loop_info[i+1].split()
           Loop_name = "Loop" + CandidFileLine[0] + "_" + CandidFileLine[1]
           run_Scoring(targetAddr, out_label, i+1, advanced_run, modeling_mode)
