#!/usr/bin/env python2.7

"""
author: Yasaman Karami
29 May 2018
MTi
"""

###########################
###  L I B R A R I E S  ###
###########################
import sys
import numpy as np
import BC
from PyPDB import PyPDB as PDB
import os
from rmsd import *
import argparse

DFLT_WORK_PATH     = "./"
DFLT_FLANK_SZE     =  4
DFLT_MODE          = "local"  #else "cluster"
PROT_DIR           = "DaReUS_Loop"
DFLT_LBL           = "dareus"
AAs                = "ARNDBCEQZGHILKMFPSTWYV"
AA3                = ("ALA","ARG","ASN","ASP","ASX","CYS","GLU","GLN","GLX","GLY","HIS","ILE","LEU","LYS","MET","PHE","PRO","SER","THR","TRP","TYR","VAL")
#########################################################################
#########################################################################
def mk_dir(name_dir):
    if not(os.path.exists(name_dir)):
        os.mkdir(name_dir)
    name_dir += '/'
    return name_dir

def prep_refe(refe_pdb):
    nb_SC, list_SC = refe_pdb.SCatmMiss()
    new_len = len(refe_pdb)
    for j in range(0,new_len):
        res_label = refe_pdb[j].rName() + '_' + refe_pdb[j].chnLbl() + '_' + refe_pdb[j].rNum() + '_' + refe_pdb[j].riCode()
        for k in range(0,len(refe_pdb[j].atms)):
            Xseg = ''
            if len(refe_pdb[j].atms[k].txt) > 79:
               before = 78
               Xseg += ' '
               tail = '\n'
            else:
               before = len(refe_pdb[j].atms[k].txt) - 1
               for ii in range(80 - len(refe_pdb[j].atms[k].txt)):
                  Xseg += ' '
               tail = '\n'
            if refe_pdb[j].rName() == "HIS" or res_label in list_SC:
               Xseg += ' '
            else:
               Xseg += 'X'
            refe_pdb[j].atms[k].txt = "%s%s%s" % (refe_pdb[j].atms[k].txt[0:before],Xseg,tail)
    return refe_pdb

def renumber(refe_pdb):
    new_len = len(refe_pdb)
    nbAtom = 1
    for i in range(0,new_len):
        refe_pdb[i].chnLbl('A')
        for k in range(0,len(refe_pdb[i].atms)):
            refe_pdb[i].atms[k].atmNum(nbAtom)
            nbAtom = nbAtom + 1
    return refe_pdb

def prep_candid(targetAddr, model_dir, flank_size, out_label):

    candid_dir = mk_dir(model_dir + "Candidates")
    candid_file1 = open(candid_dir + "data", "w")
    candid_file2 = open(candid_dir + "input_gromacs.list", "w")
    candid_file3 = open(candid_dir + "gromacs_names.list", "w")

    Loop_info = open(targetAddr + out_label + "_loops.log","r").readlines()
    for k in range(len(Loop_info)):
    	if Loop_info[k].startswith("#"): continue
    	CandidFileLine = Loop_info[k].split()
        this_Loop_seq = CandidFileLine[1]
        this_Loop_name = "Loop" + CandidFileLine[0] + "_" + this_Loop_seq
        this_Loop_size = int(CandidFileLine[2])
        this_Loop_start = int(CandidFileLine[3])
        this_Loop_before = int(CandidFileLine[4])

        gaped_pdb = PDB.PDB(model_dir + this_Loop_name + "/GappedPDB.pdb")
        refe_pdb = prep_refe(gaped_pdb)
        best_file = open(candid_dir + this_Loop_name + "_top_models.list","w")
        top_candid_file = model_dir + this_Loop_name + "/BCSearch/top_models.list"
        if not os.path.isfile(top_candid_file) or os.path.getsize(top_candid_file) <= 0:
           sys.stderr.write("No candidate was found for: %s\n" %this_Loop_name)
           continue

        top_candid = open(top_candid_file,"r").readlines()
        for j in range(len(top_candid)):
            candid_line = top_candid[j]
            candid = candid_line.split()
            name = candid[0] + "-" + candid[1] + "-" + candid[2] + "-" + candid[3] + "-" + candid[4]
            Loop_candid = PDB.PDB(model_dir + this_Loop_name + "/BCSearch/PDB_files/" + name + ".pdb")
            Loop_seg = Loop_candid[flank_size:(len(Loop_candid) - flank_size)] 
            for k in range(this_Loop_size):
                res_ind = AAs.find(this_Loop_seq[k])
                res_name = AA3[res_ind]
                Loop_seg[k].rName(res_name)
                Loop_seg[k].rNum(this_Loop_start + k)
            Loop_seg.out(model_dir + "temp_Loop.pdb")
            Loop_seg = PDB.PDB(model_dir + "temp_Loop.pdb")
            new_pdb = refe_pdb[0:(this_Loop_before+1)]+ Loop_seg + refe_pdb[(this_Loop_before+1):len(refe_pdb)]
            new_refe_pdb = renumber(new_pdb)
            new_refe_pdb.out(candid_dir + this_Loop_name + "_" + name + ".pdb")
            best_file.write(candid_line)
            candid_file1.write(this_Loop_name + "_" + name + ".pdb\n")
            candid_file2.write(this_Loop_name + "_" + name + "_model.pdb\n")
            candid_file3.write(this_Loop_name + "_" + name + "_mini\n")    
    return

def print_options(options):
    """
    @return: a string describing the options setup for the run
    """
    try:
        rs = ""
        for option in options.__dict__.keys():
            rs = "%s\n  %10s: %s" % (rs, str(option),getattr(options,option))
        return rs
    except:
        return ""

# -- ARGUMENTS --
def cmdLine():
    """
    Command line definition.
    @return: command line options and arguments (optionParser object)
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("--flank_sze", dest="flank_sze", type = int, help="Flank size (%d)" % DFLT_FLANK_SZE,
                        action="store", default=DFLT_FLANK_SZE)
    parser.add_argument("--wpath", dest="work_path", help="path to target (%s)" % DFLT_WORK_PATH,
                        action="store", default=DFLT_WORK_PATH)
    parser.add_argument("--run_mod", dest="run_mod", help="running mode (\"cluster\" or \"local\")(%s)" % DFLT_MODE,
                        action="store", default=DFLT_MODE)
    parser.add_argument("--l", dest="Lbl", help="the label for the output results.",
                        action="store", default=DFLT_LBL)
    return parser

def argParse():
    """
    Check and parse arguments.
    """
    parser = cmdLine()
    try:
        options = parser.parse_args()
    except ValueError:
        parser.error("\nInvalid command line format !\n")
    if options.work_path[-1] != "/":
        options.work_path = options.work_path + "/"
    print print_options(options)    
    return options

if __name__=="__main__":
    """
    Parse the command line and launch the main purpose.
    """
    options = argParse()
    targetAddr = options.work_path
    mode       = options.run_mod
    flank_size = options.flank_sze
    out_label  = options.Lbl

    model_dir = targetAddr + PROT_DIR + "/"
    prep_candid(targetAddr, model_dir, flank_size, out_label)



