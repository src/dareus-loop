import os
import numpy as np
from . import Real

class PDBLibraryFactory(object):
    def __call__(self, directory, databasename):
        instance = PDBLibraryInstance()
        instance.build(directory, databasename)
        return instance
PDBLibrary = PDBLibraryFactory()

class PDBLibraryInstance(object):
    pdb_index = None
    pdb_index_array = None
    seg_index = None
    seg_index_array = None
    seg_chain = None
    seg_chain_array = None
    dbbb = None
    dbca = None
    seq = None
    def build(self, directory, databasename):
        extensions = "pdbindex", "segindex", "npy"
        filenames = [directory + "/" + databasename + "." + ext for ext in extensions]
        seqfile = directory + "/" + databasename + "-SEQ.npy"
        if os.path.exists(seqfile):
            filenames.append(seqfile)
        for f in filenames:
            assert os.path.exists(f), f

        #read in pdb_index
        self.pdb_index = []
        for l in open(filenames[0]).readlines():
            if l.strip().startswith("#"): continue
            ll = l.split()
            if not len(ll): continue
            code, seg_index, nsegs  = ll[0], int(ll[1]), int(ll[2])
            self.pdb_index.append((code, seg_index, nsegs))
        self.pdb_index_array = np.array([l[1:] for l in self.pdb_index], dtype="int32")

        #read in seg_index
        self.seg_index = []
        self.seg_chain = []
        for l in open(filenames[1]).readlines():
            if l.strip().startswith("#"): continue
            ll = l.split()
            if not len(ll): continue
            dbca_offset, resnr, length, model  = int(ll[0]), int(ll[1]), int(ll[2]), int(ll[4])
            chain = ll[3]
            self.seg_index.append((dbca_offset, resnr, length, model))
            self.seg_chain.append(chain)
        self.seg_index_array = np.array(self.seg_index, dtype='int32')
        self.seg_chain_array = np.array(self.seg_chain, dtype='S1')

        self.dbbb = np.load(filenames[2])
        if self.dbbb.dtype != Real:
            self.dbbb = self.dbbb.astype(Real)
        self.dbca = np.ascontiguousarray(self.dbbb[:,1,:])
        self.seq = np.empty(len(self.dbca),dtype="|S1")
        self.seq[:] = "X"
        if len(filenames) == 4:
            self.seq[:] = np.load(filenames[3])

    def _filter(self, codes, select):
        codes = set([c.lower() for c in codes])
        pdb_index = []
        for code, seg_index, nsegs in self.pdb_index:
            keep = (not select)
            if code.lower() in codes:
                keep = (select)
            else:
                for c in codes:
                    if code.startswith(c):
                        keep = (select)
                        break
            if keep:
                pdb_index.append((code, seg_index, nsegs))
        assert len(pdb_index) #have to keep at least one PDB...
        pdb_index_array = np.array([l[1:] for l in pdb_index], dtype="int32")
        result = PDBLibraryInstance()
        result.pdb_index = pdb_index
        result.pdb_index_array = pdb_index_array
        result.seg_index = self.seg_index
        result.seg_index_array = self.seg_index_array
        result.seg_chain_array = self.seg_chain_array
        result.dbbb = self.dbbb
        result.dbca = self.dbca
        result.seq = self.seq
        return result
    def select_codes(self, codes):
        return self._filter(codes, True)
    def exclude_codes(self, codes):
        return self._filter(codes, False)
    def chunk(self, offset, step):
        pdb_index = self.pdb_index[offset::step]
        pdb_index_array = np.array([l[1:] for l in pdb_index], dtype="int32")
        result = PDBLibraryInstance()
        result.pdb_index = pdb_index
        result.pdb_index_array = pdb_index_array
        result.seg_index = self.seg_index
        result.seg_index_array = self.seg_index_array
        result.seg_chain_array = self.seg_chain_array
        result.dbbb = self.dbbb
        result.dbca = self.dbca
        result.seq = self.seq
        return result
        
        
