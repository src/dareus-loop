#ifndef HEADER_BC
#define HEADER_BC

#include <stdio.h>
#include <math.h>
#include <memory.h>
#include "common.h"
#include "util.h"

static inline Real crossBC(const Coord X[], const Coord Y[], int len) {
  //assumes that X and Y have been centered
  Real K[3][3];
  Real det;
  int i,j,k;

  memset(K, 0, sizeof(K));
  for (k=0; k<len;k++)
    for (i=0; i<3; i++)
      for (j=0; j<3;j++)
	      K[i][j]+=Y[k][i]*X[k][j];

  det=det3x3(K);
  return det;
}

static inline Real selfBC(const Coord X[], int len) {
  //assumes that X has been centered
  Real K[3][3];
  Real det;
  int i,j,k;

  memset(K, 0, sizeof(K));
  for (k=0; k<len;k++)
    for (i=0; i<3; i++)
      for (j=0; j<3;j++)
	      K[i][j]+=X[k][i]*X[k][j];

  det=det3x3(K);
  return det;
}

static inline Real fastBC(const Coord X[], const Coord Y[], int len, Real sqdetXX) {
  //calculates a BC based on the cross-BC of X and Y, the self-BC of Y, and the determinant of X
  //assumes that X and Y have been centered, and that sqrt(detXX) has been computed already

  Real detXY, detYY;
  detXY=crossBC(X, Y, len);
  detYY=selfBC(Y, len);
  if (fabs(detYY)<EPS ) {
    fprintf(stderr, "null determinant! len=%d\n",len);return 0.;
  }
  return detXY/(sqdetXX*sqrt(detYY));
}

static inline Real BC(const Coord X[], const Coord Y[], int len) {
  Coord Xc[len], Yc[len];
  Real detXY, detXX, detYY;

  center(X, len, Xc);
  center(Y, len, Yc);

  detXX = selfBC(Xc, len);
  if (fabs(detXX)<EPS ) {
    fprintf(stderr, "null determinant! len=%d\n",len);return 0.;
  }
  detYY = selfBC(Yc, len);
  if (fabs(detYY)<EPS ) {
    fprintf(stderr, "null determinant! len=%d\n",len);return 0.;
  }
  detXY = crossBC(Xc, Yc, len);

  return detXY/(sqrt(detXX)*sqrt(detYY));
}


#endif
