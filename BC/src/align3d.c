#include "common.h"
#include "score3d.h"

/*
* computes maximum score over a sliding window of length w using a dequeue.
* (without explicitely using a queue structure)
*/
int maxSlidingWindow(Real Scores[], int n, int w, Index index[]) {
  Index i,j,len;
  int back, front;
  if (n-w+1 <= 0) return 0;
  Index Q[n-w+1];
  Index I[n-w+1];

  memset(Q, 0, sizeof(Q));
  back=n-w+1;
  len=0;
  for (i=0; i<w; i++) {
    while (len>0 && Scores[i] >= Scores[Q[back]]) {
      back++;len--;
    }
    back--;Q[back]=i; len++;
  }
  front=back+len-1;
  for (i=w; i<n; i++) {
    I[i-w]=Q[front];
    while (len>0 && Scores[i] >= Scores[Q[back]]){
      back++;len--;
    }
    while (len>0 && Q[front] <= i-w){
      front--;len--;
    }
    back--;Q[back]=i;len++;
  }
  I[n-w] = Q[front];

  index[0]=I[0];j=1;
  for (i=1; i<n-w+1; i++)
  if (I[i]!=I[i-1]) index[j++]=I[i];


  /*
  printf("maxSlidingWindow:len %d %d\n", n-w+1,j);
  for (i=0; i<j; i++)
  printf("%d %d %lf\n",i,index[i],Scores[index[i]]);
  */
  return j;
}




Real deleteAl(const Coord *X,  int n1, const Coord *Y, int n2, int ndel, Index *ind1, Index *ind2, int *alen, Score3dfunc score3d) {
  int i,j,k,l,al,imin;
  Coord Xal[n1], Yal[n1];
  Index tmp1[n1], tmp2[n1];
  Real delta[n1];
  Real bc, bc0, mindelta;

  al=*alen;
  for (k=0; k<al; k++) {
    for (l=0;l<3;l++) Xal[k][l]=X[ind1[k]][l];
    for (l=0;l<3;l++) Yal[k][l]=Y[ind2[k]][l];
  }
  bc0=score3d(Xal,Yal,0,0,al);
  mindelta=1.0;
  imin=0;
  for (i=0; i<al; i++) {
    for (j=0; j<i; j++) {
      tmp1[j]=ind1[j];
      tmp2[j]=ind2[j];
    }
    for (j=i+1; j<al; j++) {
      tmp1[j-1]=ind1[j];
      tmp2[j-1]=ind2[j];
    }
    for (k=0; k<al-1; k++) {
      for (l=0;l<3;l++) Xal[k][l]=X[tmp1[k]][l];
      for (l=0;l<3;l++) Yal[k][l]=Y[tmp2[k]][l];
    }
    bc=score3d(Xal,Yal,0,0,al-1);
    delta[i]=bc0-bc;
    if (delta[i]<mindelta) {mindelta=delta[i];imin=i;}
  }

  if (ndel==0 && mindelta< -EPS) {
    for (j=imin; j<al-1; j++) {
      ind1[j]=ind1[j+1];
      ind2[j]=ind2[j+1];
    }
    al--;
  }
  if (ndel>0) {
    j=0;
    for (i=0; i<al; i++) {
      if (delta[i]> -EPS) {
        ind1[j]=ind1[i];
        ind2[j]=ind2[i];
        j++;
      }
    }
    al=j;
  }
  for (k=0; k<al; k++) {
    for (l=0;l<3;l++) Xal[k][l]=X[ind1[k]][l];
    for (l=0;l<3;l++) Yal[k][l]=Y[ind2[k]][l];
  }
  bc=score3d(Xal,Yal,0,0,al);
  *alen=al;
  return bc;
}


Real enrichAl(const Coord *X, int n1, const Coord *Y, int n2, Index *ind1, Index *ind2, int *alen, Score3dfunc score3d) {
  int i,j,k,l,iopt,jopt,al;
  Coord Xal[n1], Yal[n1];
  int lo1, hi1, lo2, hi2;
  Real bc, bcopt, bcprec;
  memset(Xal, 0, n1 * sizeof(Coord));
  memset(Yal, 0, n1 * sizeof(Coord));

  al=*alen;
  for (k=0; k<al; k++) {
    for (l=0;l<3;l++) Xal[k][l]=X[ind1[k]][l];
    for (l=0;l<3;l++) Yal[k][l]=Y[ind2[k]][l];
  }
  bcopt=score3d(Xal,Yal,0,0,al)*al;
  //printf("enrichAl::bcopt=%lf\n", bcopt);
  iopt=-1;
  bcprec=-1;
  while (bcopt>bcprec) {
    bcprec=bcopt;
    /***/
    l=0;
    while(l<=al){
      if (l==0) {lo1=0;hi1=ind1[0]-1; lo2=0; hi2=ind2[0]-1;}
      if (l==al) {lo1=ind1[l-1]+1;hi1=n1-1; lo2=ind2[l-1]+1; hi2=n2-1;}
      if (l>0 && l<al) {
        lo1=ind1[l-1]+1;hi1=ind1[l]-1;
        lo2=ind2[l-1]+1; hi2=ind2[l]-1;
      }
      /***/
      for (i=lo1; i<=hi1;i++)
      for (j=lo2; j<=hi2;j++) {
        for (k=0; k<3; k++) Xal[al][k]=X[i][k];
        for (k=0; k<3; k++) Yal[al][k]=Y[j][k];
        bc=score3d(Xal,Yal,0,0,al+1)*(al+1);
        if (bcopt<bc) {bcopt=bc; iopt=i; jopt=j;}
      }
      /***/
      if (iopt>=0) {
        bc=bcopt;
        for (k=al-1; k>=l; k--) {
          ind1[k+1]=ind1[k];
          ind2[k+1]=ind2[k];
        }
        ind1[l]=iopt;
        ind2[l]=jopt;
        al++;
        for (k=0; k<al; k++) {
          for (l=0;l<3;l++) Xal[k][l]=X[ind1[k]][l];
          for (l=0;l<3;l++) Yal[k][l]=Y[ind2[k]][l];
        }
      }
      l++;
      iopt=-1;
    }
  }
  *alen=al;
  return bcopt;
}
/*
* X query ; Y local window on target structures, longer than query (+gap)
*/
Real align3d(const Coord *Xc, int n1, const Coord *Yc, int n2, int *alen,  Index *ind1, Index *ind2, Score3dfunc score3d) {
  int k, l, al;
  Coord Xal[n1], Yal[n1];
  Real bc, bcprec;

  al=*alen;
  bc=-100;
  do {
    bcprec=bc;
    //Real score=
    deleteAl(Xc, n1, Yc, n2, 0, ind1, ind2, &al, score3d);
    //printf("score delete =%lf %d\n", score,al);
    bc=enrichAl(Xc, n1, Yc, n2,  ind1, ind2, &al, score3d);
    //printf("score enrich =%lf --> %lf %d\n", bcprec, bc,al);
  } while (bcprec<bc);
  for (k=0; k<al; k++) {
    for (l=0;l<3;l++) Xal[k][l]=Xc[ind1[k]][l];
    for (l=0;l<3;l++) Yal[k][l]=Yc[ind2[k]][l];
  }
  bc=score3d(Xal,Yal,0,0,al);
  //printf("align3d::score final=%lf from=%d %d\n", bc, ind1[0], ind2[0]);
  *alen=al;
  return bc;
}
