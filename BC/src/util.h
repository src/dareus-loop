#ifndef HEADER_UTIL
#define HEADER_UTIL

#include "common.h"
#include <math.h>

static inline void getCenter(const Coord X[], int len, Coord *cx) {
  int i,j;
  for (i=0; i<3;i++) (*cx)[i] = 0;
  for (i=0; i<len;i++)
    for (j=0; j<3;j++)
      (*cx)[j] +=X[i][j];
  for (i=0; i<3;i++) (*cx)[i] /= len;
}

static inline void center(const Coord X[], int len, Coord Xc[]) {
  Coord cx;
  int i,j;
  getCenter(X, len, &cx);
  for (i=0; i<len;i++)
    for (j=0; j<3;j++)
      Xc[i][j] = X[i][j] - cx[j];
}

static inline Real det3x3(Real K[3][3]) {
  return K[0][0]*(K[1][1]*K[2][2]-K[2][1]*K[1][2])+K[1][0]*(K[2][1]*K[0][2]-K[0][1]*K[2][2])+K[2][0]*(K[0][1]*K[1][2]-K[1][1]*K[0][2]);
}

static inline Real dist2(const Real *X,  const Real *Y) {
  Real x, sum;
  int k;
  sum=0;
  for (k=0; k<3;k++) {
    x=X[k]-Y[k];
    sum+=x*x;
  }
  // Faster not to get sqrt
  sum=sqrt(sum);
  return sum;
}

#endif
