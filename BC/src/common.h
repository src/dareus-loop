#ifndef HEADER_COMMON
#define HEADER_COMMON

typedef unsigned short Index;
typedef double Real; //TODO: test single precision?
typedef Real Coord[3];
#define EPS 1.e-8
#define min(a,b) (a<b?a:b)
#define max(a,b) (a<b?b:a)

#endif
