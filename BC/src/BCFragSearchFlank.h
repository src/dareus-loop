#include "common.h"
typedef struct {
  int pdbid; //PDB identifier of the database
  int segid; //segment identifier of the database
  Index segment_offset; //offset within segment
  char segment_chain;
  int segment_model;
  Real bc;
  Real rmsd;
  Real rigidity;
  Real looprmsd;
  Real flankrmsd;
  Real loopbc;
  Real looprigidity;
  Real flankbc;
  Real flankrigidity;
} BCFragSearchHitFlank;

int BCFragSearchFlank (const Coord *atoms, int nr_atoms, //fragment to search
              int mirror, //looking for mirrors?
              float minBC, float maxR, float maxRMSD, float maxHitRMSD, //minimum BC score, maximum rigidity, maximum RMSD
              const Coord *dbca, //CA database
              int seg_index[][4], //(dbca offset, segment resnr, segment length, model)
              char seg_chain[], //(chain)
              int pdb_index[][2], int nr_pdbindex, //(seg_index offset, number of segments), total number of PDBs
              BCFragSearchHitFlank *hits, //must be pre-allocated with size maxhits
              int maxhits
            );
