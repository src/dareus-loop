#include "common.h"
typedef struct {
  int pdbid; //PDB identifier of the database
  int segid; //segment identifier of the database
  Index segment_offset; //offset within segment
  Real rmsd;
} RMSDSearchHit;

int RMSDSearch (const Coord *atoms, int nr_atoms, //fragment to search
              float maxRMSD,
              const Coord *dbca, //CA database
              int seg_index[][4], //(dbca offset, segment resnr, segment length, model)
              char seg_chain[], //(chain)
              int pdb_index[][2], int nr_pdbindex, //(seg_index offset, number of segments), total number of PDBs
              RMSDSearchHit *hits, //must be pre-allocated with size maxhits
              int maxhits
            );
