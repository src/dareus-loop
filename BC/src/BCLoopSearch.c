/*
 * Adaptation of BCLoopSearch (routines in BCscore3_PT.c) by Sjoerd de Vries, MTi, 2016
 *
 * BCFragSearch/BCLoopSearch/BCAlign author: Frederic Guyon, MTi
 * Citations:
 *      Fast protein fragment similarity scoring using a Binet-Cauchy Kernel, Bioinformatics, Frederic Guyon  and Pierre Tuffery,  doi:10.1093/bioinformatics/btt618
 *
*/

#include "BC.h"
#include "rigidity.h"
#include "rmsd.h"
#include "BCLoopSearch.h"
#include <stdio.h>
#include <memory.h>

int BCLoopSearch (const Coord *atoms1, int nr_atoms1, const Coord *atoms2, int nr_atoms2,  //flank1 and flank2
                  int looplength, //size of the gap/loop we are searching
                  int minloopmatch, int maxloopgap, //for partial matches: minimum total length, maximum gap
                  int mirror, //looking for mirrors?
                  float minBC, float maxR, float maxRMSD,  //minimum BC score, maximum rigidity, maximum RMSD
                  const Coord *dbca, //CA database
                  int seg_index[][4], //(dbca offset, segment resnr, segment length, model)
                  char seg_chain[], //(chain)
                  int pdb_index[][2], int nr_pdbindex, //(seg_index offset, number of segments), total number of PDBs
                  BCLoopSearchHit *hits, //must be pre-allocated with size maxhits
                  int maxhits
                 )
{
  Coord X[NMAX];
  Coord Xc[NMAX];
  Coord Y[NMAX];
  Coord Yc[NMAX];
  int nX = nr_atoms1 + nr_atoms2;
  if (nX > NMAX) {
    fprintf(stderr, "NMAX exceeded! len=%d\n",nX); return -1;
  }
  memcpy(X,             atoms1, nr_atoms1 * sizeof(Coord));
  memcpy(X + nr_atoms1, atoms2, nr_atoms2 * sizeof(Coord));
  center(X, nX, Xc);
  Real detXX = selfBC(Xc, nX);
  if (fabs(detXX)<EPS ) {
    fprintf(stderr, "null determinant! len=%d\n",nX);return -1;
  }
  Real sqdetXX = sqrt(detXX);
  int totlength = nr_atoms1 + looplength + nr_atoms2;
  int pdb, seg1;
  int nhits = 0;
  for (pdb = 0; pdb < nr_pdbindex; pdb++) {
    int seg_offset = pdb_index[pdb][0];
    int nsegs = pdb_index[pdb][1];
    for (seg1 = seg_offset; seg1 < seg_offset + nsegs; seg1++) {
      int dbca_offset1 = seg_index[seg1][0];
      int seg1_first_resnr = seg_index[seg1][1];
      int seglen1 = seg_index[seg1][2];
      int seg1_model = seg_index[seg1][3];
      char seg1_chain = seg_chain[seg1];
      const Coord *dbca_seg1 = &dbca[dbca_offset1-1];
      for (int n1 = 0; n1 < seglen1; n1++) {
        if (n1 + nr_atoms1 >= seglen1) continue;
        if (seglen1 - n1 - totlength >= 0) {
          //flank1, loop and flank2 all on the same segment
          memcpy(Y,             dbca_seg1 + n1,                          nr_atoms1 * sizeof(Coord));
          memcpy(Y + nr_atoms1, dbca_seg1 + n1 + nr_atoms1 + looplength, nr_atoms2 * sizeof(Coord));
          //printf("%d\n", 0);
        }
        else if (seglen1 - n1 - nr_atoms1 - looplength >= 0) {
          //segment 1 border inside flank2 region; won't work
          continue;
        }
        else { //at least part of the gap is beyond segment 1
          int loopnonmatch = 0;
          int seg1_last_resnr = seg1_first_resnr + seglen1 - 1;
          int seg2 = seg1 + 1;
          if (seg2 - seg_offset == nsegs) {
            //last segment of this PDB; won't work
            continue;
          }
          int dbca_offset2 = seg_index[seg2][0];
          int seg2_first_resnr = seg_index[seg2][1];
          int seglen2 = seg_index[seg2][2];
          int inter_segment_gap = seg2_first_resnr - seg1_last_resnr - 1;
          if (inter_segment_gap > maxloopgap) {
            //gap too big between segments
            continue;
          }
          loopnonmatch += inter_segment_gap;
          int restgap = looplength - (seglen1 - n1 - nr_atoms1) - inter_segment_gap;
          if (restgap < 0) {
            //flank2 starts between segments; won't work
            continue;
          }
          if (seglen2 < restgap) {
            //very small segment inside the gap; let's take another one
            int seg2_old_last_resnr = seg2_first_resnr + seglen2 - 1;
            seg2 = seg2 + 1;
            if (seg2 - seg_offset == nsegs) {
              //last segment of this PDB; won't work
              continue;
            }
            dbca_offset2 = seg_index[seg2][0];
            seg2_first_resnr = seg_index[seg2][1];
            inter_segment_gap = seg2_first_resnr - seg2_old_last_resnr - 1;
            if (inter_segment_gap > maxloopgap) {
              //gap too big between segments
              continue;
            }
            loopnonmatch += inter_segment_gap;
            restgap = restgap - seglen2 - inter_segment_gap;
            seglen2 = seg_index[seg2][2];
            if (restgap < 0) {
              //flank2 starts between segments; won't work
              continue;
            }
            if (seglen2 < restgap) {
              //another very small segment inside the gap; give up
              continue;
            }
          }
          int n2 = restgap;
          if (n2 +  nr_atoms2 > seglen2) {
            //flank2 does not fit in the rest of segment 2; won't work
            continue;
          }
          if (looplength - loopnonmatch <  minloopmatch) {
            //we didn't match enough of the loop
            continue;
          }
          const Coord *dbca_seg2 = &dbca[dbca_offset2-1];
          memcpy(Y,             dbca_seg1 + n1, nr_atoms1 * sizeof(Coord));
          memcpy(Y + nr_atoms1, dbca_seg2 + n2, nr_atoms2 * sizeof(Coord));
        }

        //OK, we got our X and Y coordinate arrays... is the match any good?
        center(Y, nX, Yc);
        float score0 = fastBC(Xc, Yc, nX, sqdetXX);
        if ((!mirror) && (score0 <minBC)) continue;
        if (mirror && (score0 >-minBC)) continue;
        float rigid=rigidity(Xc,Yc,nX);
        if (maxR > -1) {
          if (rigid>maxR) continue;
        }

        //RMSD check
        Real rms = -1;
        if (maxRMSD > -1) {
          rmsd(Xc,Yc,0,0,nX, &rms);
          if (rms>maxRMSD) continue;
        }

        if (nhits == maxhits) {
          fprintf(stderr, "MAXHITS reached!"); return maxhits;
        }

        BCLoopSearchHit *hit = &hits[nhits];
        hit->pdbid = pdb;
        hit->segid = seg1;
        hit->segment_offset = n1;
        hit->segment_chain = seg1_chain;
        hit->segment_model = seg1_model;
        hit->bc = score0;
        hit->rigidity = rigid;
        nhits++;
      }
    }
  }
  return nhits;
}
