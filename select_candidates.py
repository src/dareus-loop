#!/usr/bin/env python2.7

"""
author: Yasaman Karami
29 May 2018
MTi
"""

from __future__ import print_function

###########################
###  L I B R A R I E S  ###
###########################
import sys
import numpy as np
import BC
from PyPDB import PyPDB as PDB
import os
from rmsd import *
import argparse
import multiprocessing as mp
from scipy.spatial import cKDTree as KDTree

DFLT_WORK_PATH     = "./"
DFLT_FLANK_SZE     =  4
PROT_DIR           = "DaReUS_Loop"
DFLT_LBL           = "dareus"

CLASH_THRESHOLD = 3

#########################################################################
#########################################################################

def find_clash(loop_bb, clash_body_tree):
    FinalClashes = 0
    if clash_body_tree is not None:
        clashes = clash_body_tree.query_ball_point(loop_bb,CLASH_THRESHOLD)
        for l in clashes:
          for x in l:
            FinalClashes = FinalClashes + 1
    return FinalClashes

def read_and_score(Waddr, RMSDvallines, flank_size, rep_top, check_clashes):
    refe_pdb = PDB.PDB(Waddr + "GappedPDB.pdb")
    refe_ca = get_ca(refe_pdb)
    refe_KDtree = KDTree(refe_ca)
    JSD_scores = []
    RMSD_scores = []
    score_indices = []
    for index, RMSDvalline in enumerate(RMSDvallines):
        if RMSDvalline.startswith("#"): continue
        candid = RMSDvalline.split()
        name = candid[0] + "-" + candid[1] + "-" + candid[2] + "-" + candid[3] + "-" + candid[4]
        ### check for clashes
        Loop_candid = PDB.PDB(Waddr + "BCSearch/PDB_files/" + name + ".pdb")
        Loop_seg = Loop_candid[flank_size:(len(Loop_candid) - flank_size)]
        Loop_bb = get_bb(Loop_seg)
        if len(Loop_bb) < (len(Loop_seg) * 2):
            continue ### missing more than half of the backbone atoms
        Loop_ca = get_ca(Loop_seg)
        if check_clashes:
            clash_val = find_clash(Loop_ca, refe_KDtree)
            if clash_val > 0:
                continue
        ### read JSD scores
        JSD_score = np.inf
        JSD_score_file = Waddr + "BCSearch/PyPPP_score_" + name + ".txt"
        if not os.path.exists(JSD_score_file):
            print("Warning: JSD score for %s does not exist" % name, file=sys.stderr)
        else:
            JSD_score = float(open(JSD_score_file, "r").readlines()[0].split()[-2])
        score_indices.append(index)
        JSD_scores.append(JSD_score)
        RMSD_scores.append(float(candid[7])) # flank RMSD

    score_indices = np.array(score_indices,dtype=int)
    JSD_scores = np.array(JSD_scores,dtype=float)
    RMSD_scores = np.array(RMSD_scores,dtype=float)
    JSD_sorted_indices = score_indices[np.argsort(JSD_scores)]
    RMSD_sorted_indices = score_indices[np.argsort(RMSD_scores)]
    JSD_scores.sort()
    RMSD_scores.sort()

    top_ind = []
    for i in range(min(rep_top, len(score_indices))):
        curr_JSD = JSD_scores[i]
        if curr_JSD != np.inf:
            curr_JSD_index = JSD_sorted_indices[i]
            if curr_JSD_index not in top_ind:  # slow for extremely large numbers of candidates
                top_ind.append(curr_JSD_index)
        curr_RMSD = RMSD_scores[i]
        if curr_RMSD != np.inf:  # np.inf shouldn't happen, but you never know
            curr_RMSD_index = RMSD_sorted_indices[i]
            if curr_RMSD_index not in top_ind:  # slow for extremely large numbers of candidates
                top_ind.append(curr_RMSD_index)

    top_ind = np.array(top_ind,dtype='int32')
    return top_ind

def Join_refe_candid(RMSDfile, Loop_addr, flank_size, top, check_clashes):
    candid_file4 = open(Loop_addr + "BCSearch/top_models.list", "w")
    RMSDvallines = open(RMSDfile,"r").readlines()
    top_ind = read_and_score(Loop_addr, RMSDvallines, flank_size, top, check_clashes)
    for j1 in top_ind:
        if RMSDvallines[j1].startswith("#"): continue
        linee = RMSDvallines[j1].split()
        Name = linee[0] + "-" + linee[1] + "-" + linee[2] + "-" + linee[3] + "-" + linee[4]
        PyPPP_score_file = Loop_addr + "BCSearch/PyPPP_score_" + Name + ".txt"
        PyPPP_score = np.inf
        if os.path.exists(PyPPP_score_file):
            PyPPP_score = float(open(PyPPP_score_file, "r").readlines()[0].split()[-2])
        candid_file4.write("%s %0.2f\n" %(RMSDvallines[j1].split("\n")[0], PyPPP_score))
    return len(top_ind)


def Prep_Candidates(model_dir, loop_name, flank_size, top, check_clashes):
    gap_dir = model_dir + loop_name + "/"
    Loop_addr = gap_dir + "BCSearch/"
    BC_final_result = Loop_addr + "BCLoopCluster_Final_BC.txt"
    hits = 0
    if(os.path.exists(BC_final_result) and len(open(BC_final_result,"r").read())>0) :
        hits = Join_refe_candid(BC_final_result, gap_dir, flank_size, top, check_clashes)
    if not hits:
        sys.stderr.write("No hit was found for loop %s\n" %loop_name)
    return

def Prep_Candidates_all(Loop_info, model_dir, flank_size, top, check_clashes):
    nbLoop = len(Loop_info) - 1
    for k in range(nbLoop):
        CandidFileLine = Loop_info[k+1].split()
        LoopSeq = CandidFileLine[1]
        Loop_name = "Loop" + CandidFileLine[0] + "_" + LoopSeq
        print(Loop_name, file=sys.stderr)
        Prep_Candidates(model_dir, Loop_name, flank_size, top, check_clashes)

def print_options(options):
    """
    @return: a string describing the options setup for the run
    """
    try:
        rs = ""
        for option in options.__dict__.keys():
            rs = "%s\n  %10s: %s" % (rs, str(option),getattr(options,option))
        return rs
    except:
        return ""

# -- ARGUMENTS --
def cmdLine():
    """
    Command line definition.
    @return: command line options and arguments (optionParser object)
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("--flank_sze", dest="flank_sze", type = int, help="Flank size (%d)" % DFLT_FLANK_SZE,
                        action="store", default=DFLT_FLANK_SZE)
    parser.add_argument("--wpath", dest="work_path", help="path to target (%s)" % DFLT_WORK_PATH,
                        action="store", default=DFLT_WORK_PATH)
    parser.add_argument("--l", dest="Lbl", help="the label for the output results.",
                        action="store", default=DFLT_LBL)
    parser.add_argument("--top", help="""top structures to select.
A structure is selected if it belongs to the top in JSD *or* to the top in flank RMSD""", required=True,type=int)
    parser.add_argument("--check_clashes", help="check for clashes", action="store_true")
    return parser

def argParse():
    """
    Check and parse arguments.
    """
    parser = cmdLine()
    try:
        options = parser.parse_args()
    except ValueError:
        parser.error("\nInvalid command line format !\n")
    if options.work_path[-1] != "/":
        options.work_path = options.work_path + "/"
    print(print_options(options))
    return options

if __name__=="__main__":
    """
    Parse the command line and launch the main purpose.
    """
    options = argParse()
    targetAddr = options.work_path
    flank_size = options.flank_sze
    out_label  = options.Lbl
    top = options.top
    check_clashes = options.check_clashes

    model_dir = targetAddr + PROT_DIR + "/"
    Loop_info = open(targetAddr + out_label + "_loops.log","r").readlines()

    Prep_Candidates_all(Loop_info, model_dir, flank_size, top, check_clashes)
