#!/usr/bin/env python2.7

"""
author: Yasaman Karami
5 July 2018
MTi
"""
###########################
###  L I B R A R I E S  ###
###########################
import sys
import numpy as np
from PyPDB import PyPDB as PDB
import sys, os
import argparse
from Bio import pairwise2
from rmsd import *
from scipy.spatial import cKDTree as KDTree

DFLT_WORK_PATH     = "./"
PROT_DIR           = "DaReUS_Loop"
DFLT_FLANK_SZE     = 4
DFLT_MODE          = "local"  #else "cluster"
DFLT_LBL           = "dareus"
DFLT_ADV           = 0
#########################################################################
#########################################################################
def find_clash(loop_bb, clash_body_tree, Clash_th):
    FinalClashes = 0
    if clash_body_tree is not None:
        clashes = clash_body_tree.query_ball_point(loop_bb,Clash_th)
        for l in clashes:
          for x in l:
            FinalClashes = FinalClashes + 1
    return FinalClashes

def run_Scoring(targetAddr, out_label, nb_line, advanced_run, modeling_mode):
    '''
    selecting and writing the final top 10 candidates for loop #nb_line
    top 5 candidates based on JSD and
    top 5 candidates based on flank RMSD
    '''
    Clash_th = 3
    nb_top = 10
    model_dir = targetAddr + PROT_DIR + "/"
    ### current loop
    Loop_info = open(targetAddr + out_label + "_loops.log","r").readlines()
    linee = Loop_info[nb_line].split()
    candid_dir = model_dir + "Candidates/"
    this_loop_name = "Loop%d_%s" %(int(linee[0]), linee[1])
    loop_dir = model_dir + this_loop_name + "/BCSearch/"
    #### refe pdb
    this_Loop_before = int(linee[4])
    this_Loop_start = int(linee[3])
    this_Loop_size = int(linee[2])
    if advanced_run == 1 or modeling_mode == "ReModeling":
        gaped_pdb = PDB.PDB(model_dir + this_loop_name + "/GappedPDB.pdb")
        start_ind = this_Loop_before + 1
        end_ind = start_ind + this_Loop_size
    else: 
        start_ind = this_Loop_start - 1
        end_ind = start_ind + this_Loop_size
        gaped_refe_pdb = PDB.PDB(model_dir + "refe_str_no_gap.pdb")
        for ii in range(len(gaped_refe_pdb)):
            if int(gaped_refe_pdb[ii].rNum()) == start_ind:
               Rstart_ind = ii
            if int(gaped_refe_pdb[ii].rNum()) == (end_ind+1) :
               Rend_ind = ii
        start_ind = Rstart_ind + 1
        end_ind = Rend_ind
        gaped_pdb = gaped_refe_pdb[0 : start_ind] + gaped_refe_pdb[end_ind : len(gaped_refe_pdb)]
    refe_bb = get_ca(gaped_pdb)
    ##### writing final results
    all_models = open(targetAddr + out_label + "_" + this_loop_name + "_models.pdb", "w")
    table_file = open(targetAddr + out_label + "_" + this_loop_name + "_summary.txt", "w") 
    table_file.write("#Name pdb_entry chain model start end flank_BCscore flank_rigidity flank_RMSD JSD_mean Sequence\n")
    score_file = candid_dir + this_loop_name + "_top_models.list"
    if not os.path.isfile(score_file) or os.path.getsize(score_file) <= 0 :
        return
    ################ Scoring (Flank RMSD, JSD) ###########################################################
    ScoreLine = open(score_file,"r").readlines()
    nbCandid = len(ScoreLine)
    nb_top = min(nbCandid,nb_top)
    j = 0; nbb = 0
    while j < nbCandid and nbb < nb_top:
        linee = ScoreLine[j]
        j+= 1
        candid = linee.split()
        name = candid[0] + "-" + candid[1] + "-" + candid[2] + "-" + candid[3] + "-" + candid[4]
        candid_file = candid_dir + this_loop_name + "_" + name + "_mini.pdb"
        candid_file_unref = candid_dir + this_loop_name + "_" + name + "_model.pdb"
        if not os.path.exists(candid_file):
            if not os.path.exists(candid_file_unref): continue
            candid_file = candid_file_unref
        model_name = out_label + "_" + this_loop_name + "_model" + str(nbb+1) + ".pdb"
        ################################################################
        # check for clashes 
        hit_PDB = PDB.PDB(candid_file)
        Loop_seg = hit_PDB[start_ind:end_ind]
        body_seg = hit_PDB[0:start_ind] + hit_PDB[end_ind:len(hit_PDB)]
        body_ca = get_ca(body_seg)
        body_KDtree = KDTree(body_ca)
        Loop_ca = get_ca(Loop_seg)
        clash_val = find_clash(Loop_ca, body_KDtree, Clash_th)
        sys.stderr.write(" %s %s %d" %(this_loop_name, name, clash_val))
        if clash_val > 0: continue
        ################################################################
        table_file.write("%s %s %s %s %s %s %0.2f %0.2f %0.2f %0.2f %s\n" %(model_name, candid[0], candid[1], candid[2], candid[3], candid[4], float(candid[5]), float(candid[6]), float(candid[7]), float(candid[12]), candid[11]))
        ### align to refe
        #hit_PDB = PDB.PDB(candid_file)
        candid_all = get_all(hit_PDB)
        new_pdb = hit_PDB[0 : start_ind] + hit_PDB[end_ind : len(hit_PDB)]
        candid_sub_bb = get_ca(new_pdb)
        rotmat, offset, rmsdAll = fit(refe_bb, candid_sub_bb)
        pivot = np.sum(candid_sub_bb,axis=0) / len(candid_sub_bb)
        new_all = apply_matrix(candid_all, pivot, rotmat, offset) 
        candid_new_pdb = update_all(hit_PDB, new_all) 
        #candid_new_pdb.out(targetAddr + model_name)
        candid_new_pdb.out(model_dir + model_name)
        candid_new_pdb = open(model_dir + model_name, "r").readlines()
        all_models.write("MODEL        %d \n" %(nbb+1))
	for ii in range(len(candid_new_pdb)):
            all_models.write(candid_new_pdb[ii])
        all_models.write("ENDMDL\n")
        nbb += 1
    table_file.close()
    all_models.close()
    return

def print_options(options):
    """
    @return: a string describing the options setup for the run
    """
    try:
        rs = ""
        for option in options.__dict__.keys():
            rs = "%s\n  %10s: %s" % (rs, str(option),getattr(options,option))
        return rs
    except:
        return ""


# -- ARGUMENTS --
def cmdLine():
    """
    Command line definition.

    @return: command line options and arguments (optionParser object)
    """

    parser = argparse.ArgumentParser()
    
    parser.add_argument("--wpath", dest="work_path", help="path to target (%s)" % DFLT_WORK_PATH,
                        action="store", default=DFLT_WORK_PATH)

    parser.add_argument("--run_mod", dest="run_mod", help="running mode (\"cluster\" or \"local\")(%s)" % DFLT_MODE,
                        action="store", default=DFLT_MODE)

    parser.add_argument("--l", dest="Lbl", help="the label for the output results.",
                        action="store", default=DFLT_LBL)

    parser.add_argument("--advanced", dest="advanced", type = int, help="Modeling loops independently!",
                        action="store", default=DFLT_ADV)

    parser.add_argument("--mode", dest="mode", help="Modeling or Re-Modeling",
                        action="store", default = None)


    
    return parser


def argParse():
    """
    Check and parse arguments.
    """

    parser = cmdLine()

    try:
        options = parser.parse_args()
    except ValueError:
        parser.error("\nInvalid command line format !\n")

    if options.work_path[-1] != "/":
        options.work_path = options.work_path + "/"
        
    print print_options(options)    
        
    return options


if __name__=="__main__":
    """
    Parse the command line and launch the main purpose.
    """

    options = argParse()

    targetAddr    = options.work_path
    mode          = options.run_mod
    out_label     = options.Lbl
    advanced_run  = options.advanced
    modeling_mode = options.mode

    Loop_info = open(targetAddr + out_label + "_loops.log","r").readlines()
    nbLoop = len(Loop_info) - 1
    if mode == "cluster":
        nb_line = int(os.environ['TASK_ID'])
        if Loop_info[nb_line].startswith("#"):
           sys.stderr.write("wrong loop info!\n")
        CandidFileLine = Loop_info[nb_line].split()
        Loop_name = "Loop" + CandidFileLine[0] + "_" + CandidFileLine[1]
        run_Scoring(targetAddr, out_label, nb_line, advanced_run, modeling_mode)
    else:
        for i in range(nbLoop):
           CandidFileLine = Loop_info[i+1].split()
           Loop_name = "Loop" + CandidFileLine[0] + "_" + CandidFileLine[1]
           run_Scoring(targetAddr, out_label, i+1, advanced_run, modeling_mode)
