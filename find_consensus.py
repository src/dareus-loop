#!/usr/bin/env python2.7

"""
author: Yasaman Karami
5 July 2018
MTi
"""
###########################
###  L I B R A R I E S  ###
###########################
import sys
import numpy as np
from PyPDB import PyPDB as PDB
import sys, os
import argparse
from Bio import pairwise2
from rmsd import *

DFLT_WORK_PATH     = "./"
PROT_DIR           = "DaReUS_Loop"
DFLT_FLANK_SZE     = 4
DFLT_LBL           = "dareus"
DFLT_MODE          = "local"  #else "cluster"
AAs                = "ARNDBCEQZGHILKMFPSTWYV"
AA3                = ("ALA","ARG","ASN","ASP","ASX","CYS","GLU","GLN","GLX","GLY","HIS","ILE","LEU","LYS","MET","PHE","PRO","SER","THR","TRP","TYR","VAL")
#########################################################################
#########################################################################
def read_loops_info(model_dir, Loop_info):
    nbLoop = len(Loop_info) - 1
    Loops = np.zeros((nbLoop,3))
    seq=[]; ind = 0; Loop_name=[]
    Loop_size = np.zeros(nbLoop,dtype='int32')
    nbCandid = np.zeros(nbLoop, dtype='int32')
    for i in range(len(Loop_info)):
        if Loop_info[i].startswith("#"): continue
        CandidFileLine = Loop_info[i].split()
        Loops[ind,0] = int(CandidFileLine[3]) - 1  #Loop start
        Loops[ind,1] = int(CandidFileLine[2]) + Loops[ind,0]  #Loop stop
        Loops[ind,2] = int(CandidFileLine[4]) # res_before_gap
        Loop_size[i-1] = int(CandidFileLine[2])
        seq.append(CandidFileLine[1])
        Loop_name.append("Loop" + CandidFileLine[0] + "_" + CandidFileLine[1])
        top_file = model_dir + "Loop" + CandidFileLine[0] + "_" + CandidFileLine[1] + "/BCSearch/top_models.list"
        if os.path.exists(top_file):
           nbCandid[ind] = len(open(top_file,"r").readlines())
        ind += 1
    return Loops[0:ind,], seq, Loop_name, Loop_size,nbCandid

def find_loop_seg(model_dir, Loop_name, flank_size, candid):
    LoopPDB = PDB.PDB(model_dir + Loop_name + "/BCSearch/PDB_files/" + candid[0] + "-" + candid[1] + "-" + candid[2] + "-" + candid[3] + "-" + candid[4] + ".pdb")
    Loop_seg = LoopPDB[flank_size:(len(LoopPDB) - flank_size)]
    Loop_seg_ca = get_ca(Loop_seg)
    return Loop_seg_ca

def renumber(refe_pdb):
    new_len = len(refe_pdb)
    nbAtom = 1
    for i in range(0,new_len):
        #refe_pdb[i].rNum(i+1)
        refe_pdb[i].chnLbl('A')
        for k in range(0,len(refe_pdb[i].atms)):
            refe_pdb[i].atms[k].atmNum(nbAtom)
            nbAtom = nbAtom + 1
    return refe_pdb

def prep_refe(refe_pdb):
    nb_SC, list_SC = refe_pdb.SCatmMiss()
    new_len = len(refe_pdb)
    for j in range(0,new_len):
        res_label = refe_pdb[j].rName() + '_' + refe_pdb[j].chnLbl() + '_' + refe_pdb[j].rNum() + '_' + refe_pdb[j].riCode()
        for k in range(0,len(refe_pdb[j].atms)):
            Xseg = ''
            if len(refe_pdb[j].atms[k].txt) > 79:
               before = 78
               Xseg += ' '
               tail = '\n'
            else:
               before = len(refe_pdb[j].atms[k].txt) - 1
               for ii in range(80 - len(refe_pdb[j].atms[k].txt)):
                  Xseg += ' '
               tail = '\n'
            if refe_pdb[j].rName() == "HIS" or res_label in list_SC:
               Xseg += ' '
            else:
               Xseg += 'X'
            refe_pdb[j].atms[k].txt = "%s%s%s" % (refe_pdb[j].atms[k].txt[0:before],Xseg,tail)
    return refe_pdb

def select_refe(targetAddr, model_dir, out_label, flank_size):
    '''
    finding the consensus structure, with loops that do not clash
    selecting top 10 candidates per loop
    '''
    gaped_pdb = PDB.PDB(model_dir + "Gapped_renumbered.pdb")
    clash_mat_file = model_dir + "clashes.npy"
    ### all the Loops
    Loop_info = open(targetAddr + out_label + "_loops.log","r").readlines()
    Loops_id, seq, Loops_name, Loop_size, nbCandid = read_loops_info(model_dir, Loop_info)
    nbLoops = len(seq)
    # sort the loops from smaller to larger
    sort_ind = np.argsort(Loop_size)
    clash_matrix = np.load(clash_mat_file)
    refe_str = {}
    for i in range(0,nbLoops):
        refe_str[Loops_name[i]] = -1
    # find one reference model for each loop (the first top model having no clashes with the rest)
    this_Loop_name = Loops_name[sort_ind[0]]
    refe_str[this_Loop_name] = 0
    for i in range(1,nbLoops):
        ind1 = sort_ind[i]
        Loop_name1 = Loops_name[ind1]
        C1 = 0
        while C1 < nbCandid[ind1] and refe_str[Loop_name1] == -1 :
            no_clashes = 0
            for j in range(0,i):
                ind2 = sort_ind[j]
                Loop_name2 = Loops_name[ind2]
                C2 = refe_str[Loop_name2]
                if clash_matrix[ind1][ind2][C1][C2] == 0 :
                   no_clashes += 1
            if no_clashes == i:
               refe_str[Loop_name1] = C1
            C1 += 1

    # completing the reference structure
    refe_pdb = prep_refe(gaped_pdb)
    gaps = 0; gaps2 = 0
    for i in range(0,nbLoops):
        this_Loop_name = Loops_name[i]
        this_Loop_start = Loops_id[i,0]
        ind = refe_str[this_Loop_name]
        s2 =int(Loops_id[i,2]) + gaps2 + 1
        gaps2 += Loop_size[i]
        if ind == -1: continue  #There is no candidate for this loop
        top_models = model_dir + this_Loop_name + "/BCSearch/top_models.list"
        if not os.path.exists(top_models): continue  #There is no candidate for this loop
        top_models = open(top_models,"r").readlines()
        if not len(top_models): continue  #There is no candidate for this loop
        candid = top_models[ind].split()
        Loop_candid = PDB.PDB(model_dir + this_Loop_name + "/BCSearch/PDB_files/" + candid[0] + "-" + candid[1] + "-" + candid[2] + "-" + candid[3] + "-" + candid[4] + ".pdb")
        Loop_seg = Loop_candid[flank_size:(len(Loop_candid) - flank_size)]
        s1 = int(Loops_id[i,2]) + gaps + 1
        gaps += Loop_size[i]
        for j in range(Loop_size[i]):
            res_ind = AAs.find(seq[i][j])
            res_name = AA3[res_ind]
            Loop_seg[j].rName(res_name)
            Loop_seg[j].rNum(this_Loop_start + j + 1) #s2+j+1
        Loop_seg.out(model_dir + "temp_Loop.pdb")
        Loop_seg = PDB.PDB(model_dir + "temp_Loop.pdb")
        new_pdb = refe_pdb[0:s1] + Loop_seg + refe_pdb[s1:len(refe_pdb)]
        refe_pdb = new_pdb
    refe_pdb = renumber(refe_pdb)
    refe_pdb.out(model_dir + "refe_str_no_gap.pdb")
    return

def print_options(options):
    """
    @return: a string describing the options setup for the run
    """
    try:
        rs = ""
        for option in options.__dict__.keys():
            rs = "%s\n  %10s: %s" % (rs, str(option),getattr(options,option))
        return rs
    except:
        return ""


# -- ARGUMENTS --
def cmdLine():
    """
    Command line definition.

    @return: command line options and arguments (optionParser object)
    """

    parser = argparse.ArgumentParser()

    parser.add_argument("--wpath", dest="work_path", help="path to target (%s)" % DFLT_WORK_PATH,
                        action="store", default=DFLT_WORK_PATH)

    parser.add_argument("--run_mod", dest="run_mod", help="running mode (\"cluster\" or \"local\")(%s)" % DFLT_MODE,
                        action="store", default=DFLT_MODE)

    parser.add_argument("--flank_sze", dest="flank_sze", type = int, help="Flank size (%d)" % DFLT_FLANK_SZE,
                        action="store", default=DFLT_FLANK_SZE)

    parser.add_argument("--l", dest="Lbl", help="the label for the output results.",
                        action="store", default=DFLT_LBL)


    return parser


def argParse():
    """
    Check and parse arguments.
    """

    parser = cmdLine()

    try:
        options = parser.parse_args()
    except ValueError:
        parser.error("\nInvalid command line format !\n")

    if options.work_path[-1] != "/":
        options.work_path = options.work_path + "/"

    print print_options(options)

    return options


if __name__=="__main__":
    """
    Parse the command line and launch the main purpose.
    """

    options = argParse()

    targetAddr = options.work_path
    mode       = options.run_mod
    flank_size = options.flank_sze
    out_label  = options.Lbl

    model_dir = targetAddr + PROT_DIR + "/"
    select_refe(targetAddr, model_dir, out_label, flank_size)
