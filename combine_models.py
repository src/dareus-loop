#!/usr/bin/env python2.7

"""
author: Yasaman Karami
5 July 2018
MTi
"""
###########################
###  L I B R A R I E S  ###
###########################
import sys
import numpy as np
from PyPDB import PyPDB as PDB
import sys, os
import argparse
from Bio import pairwise2
from rmsd import *
from scipy.spatial import cKDTree as KDTree

DFLT_WORK_PATH     = "./"
PROT_DIR           = "DaReUS_Loop"
DFLT_FLANK_SZE     = 4
DFLT_MODE          = "local"  #else "cluster"
DFLT_LBL           = "dareus"
DFLT_ADV           = 0
#########################################################################
#########################################################################
def run_Scoring(targetAddr, protName, out_label):
    '''
    generating final model combinations
    '''
    model_dir = targetAddr + PROT_DIR + "/"
    Loop_info = open(targetAddr + out_label + "_loops.log","r").readlines()
    nbLoop = len(Loop_info) - 1
    if advanced_run == 1 or modeling_mode == "ReModeling":
        gaped_refe_pdb = PDB.PDB(targetAddr + protName)
    else:
        gaped_refe_pdb = PDB.PDB(model_dir + "refe_str_no_gap.pdb")

    for i in range(nbLoop):
        if Loop_info[i].startswith("#"): continue
        linee = Loop_info[i+1].split()
        this_loop_name = "Loop%d_%s" %(int(linee[0]), linee[1])
        this_Loop_before = int(linee[4])
        this_Loop_start = int(linee[3])
        this_Loop_size = int(linee[2])
        ### start and end ind
        start_ind = this_Loop_start - 1
        end_ind = start_ind + this_Loop_size
        for ii in range(len(gaped_refe_pdb)):
            if int(gaped_refe_pdb[ii].rNum()) == start_ind:
                Rstart_ind = ii
            if int(gaped_refe_pdb[ii].rNum()) == (end_ind+1) :
                Rend_ind = ii
        start_ind = Rstart_ind + 1
        end_ind = Rend_ind
        ##### writing final results
        all_models_name = targetAddr + out_label + "_" + this_loop_name + "_models.pdb"
        if not os.path.isfile(all_models_name) or os.path.getsize(all_models_name) <= 0 : continue
        this_loop_models = PDB.PDB(all_models_name)
        this_loop_models.setModel(1)
        sub_seg = this_loop_models[start_ind:end_ind]
        new_pdb = gaped_refe_pdb[0:start_ind] + sub_seg + gaped_refe_pdb[end_ind:len(gaped_refe_pdb)]
        gaped_refe_pdb = new_pdb
    for i in range(len(gaped_refe_pdb)):
        gaped_refe_pdb[i].chnLbl("A")
    out_name = targetAddr + out_label + "_final_models.pdb"
    gaped_refe_pdb.out(out_name)
    return

def print_options(options):
    """
    @return: a string describing the options setup for the run
    """
    try:
        rs = ""
        for option in options.__dict__.keys():
            rs = "%s\n  %10s: %s" % (rs, str(option),getattr(options,option))
        return rs
    except:
        return ""


# -- ARGUMENTS --
def cmdLine():
    """
    Command line definition.

    @return: command line options and arguments (optionParser object)
    """

    parser = argparse.ArgumentParser()
    
    parser.add_argument("--wpath", dest="work_path", help="path to target (%s)" % DFLT_WORK_PATH,
                        action="store", default=DFLT_WORK_PATH)

    parser.add_argument("--model_str", dest="model_str", help="Modeling: gaped structure, Remodeling: initial model",
                        action="store", required=True, default=None)

    parser.add_argument("--run_mod", dest="run_mod", help="running mode (\"cluster\" or \"local\")(%s)" % DFLT_MODE,
                        action="store", default=DFLT_MODE)

    parser.add_argument("--l", dest="Lbl", help="the label for the output results.",
                        action="store", default=DFLT_LBL)

    parser.add_argument("--advanced", dest="advanced", type = int, help="Modeling loops independently!",
                        action="store", default=DFLT_ADV)

    parser.add_argument("--mode", dest="mode", help="Modeling or Re-Modeling",
                        action="store", default = None)


    
    return parser


def argParse():
    """
    Check and parse arguments.
    """

    parser = cmdLine()

    try:
        options = parser.parse_args()
    except ValueError:
        parser.error("\nInvalid command line format !\n")

    if options.work_path[-1] != "/":
        options.work_path = options.work_path + "/"
        
    print print_options(options)    
        
    return options


if __name__=="__main__":
    """
    Parse the command line and launch the main purpose.
    """

    options = argParse()

    targetAddr    = options.work_path
    protName      = options.model_str
    mode          = options.run_mod
    out_label     = options.Lbl
    advanced_run  = options.advanced
    modeling_mode = options.mode

    run_Scoring(targetAddr, protName, out_label)
