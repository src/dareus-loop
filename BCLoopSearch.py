#!/usr/bin/env python2.7

"""
author: Yasaman Karami
5 July 2018
MTi
"""

###########################
###  L I B R A R I E S  ###
###########################
import numpy as np
import BC
from BC import PDBLibrary
from BC import BCLoopSearch
from PyPDB import PyPDB as PDB
import sys, os
from rmsd import *
from scipy.cluster.hierarchy import fcluster
from scipy.cluster.hierarchy import dendrogram, linkage
import scipy.cluster.hierarchy as hcl
from scipy.spatial.distance import squareform
import argparse
import multiprocessing as mp
import warnings
warnings.filterwarnings("always")

DFLT_WORK_PATH     = "./"
DFLT_BANK          = "/scratch/banks/bc_banks"
DFLT_PDB_PATH      = DFLT_BANK + "/pdbChainID/"
DFLT_PDB_NAME      = "pdbChainID"
DFLT_RIGIDITY_MAX  = 3.
DFLT_FLANK_SZE     = 4
DFLT_RMSD_MAX      = 4.
DFLT_MODE          = "local"  #else "cluster"
AAs                = "ARNDCQEGHILKMFPSTWYVBZX*"
PROT_DIR           = "DaReUS_Loop"
DFLT_LBL           = "dareus"
#########################################################################
#########################################################################
AAs = "ARNDCQEGHILKMFPSTWYVBZX*"
blosum62 = [
      [ 4, -1, -2, -2,  0, -1, -1,  0, -2, -1, -1, -1, -1, -2, -1,  1,  0, -3, -2,  0, -2, -1,  0, -4],
      [-1,  5,  0, -2, -3,  1,  0, -2,  0, -3, -2,  2, -1, -3, -2, -1, -1, -3, -2, -3, -1,  0, -1, -4],
      [-2,  0 , 6 , 1 ,-3,  0,  0,  0,  1, -3, -3,  0, -2, -3, -2,  1,  0, -4, -2, -3,  3,  0, -1, -4 ],
      [-2, -2 , 1 , 6 ,-3,  0,  2, -1, -1, -3, -4, -1, -3, -3, -1,  0, -1, -4, -3, -3,  4,  1, -1, -4 ],
      [ 0, -3 ,-3 ,-3  ,9 ,-3, -4 ,-3, -3, -1, -1, -3, -1, -2, -3, -1, -1, -2, -2, -1, -3, -3, -2, -4 ],
      [-1,  1 , 0 , 0 ,-3,  5 , 2, -2,  0, -3, -2,  1,  0, -3, -1,  0, -1, -2, -1, -2,  0,  3, -1, -4 ],
      [-1,  0 , 0 , 2 ,-4,  2 , 5, -2,  0, -3, -3,  1, -2, -3, -1,  0, -1, -3, -2, -2,  1,  4, -1, -4 ],
      [ 0 ,-2  ,0 ,-1 ,-3 ,-2 ,-2,  6 ,-2, -4, -4, -2, -3, -3, -2,  0, -2, -2, -3, -3, -1, -2, -1, -4 ],
      [-2 , 0 , 1 ,-1 ,-3,  0,  0, -2,  8, -3, -3, -1, -2, -1, -2, -1, -2, -2,  2, -3,  0,  0, -1, -4 ],
      [-1 ,-3 ,-3 ,-3 ,-1 ,-3, -3, -4, -3,  4,  2, -3,  1,  0, -3, -2, -1, -3, -1,  3, -3, -3, -1, -4 ],
      [-1 ,-2 ,-3 ,-4 ,-1, -2, -3, -4, -3,  2,  4, -2,  2,  0, -3, -2, -1, -2, -1,  1, -4, -3, -1, -4 ],
      [-1 , 2 , 0 ,-1 ,-3,  1,  1, -2, -1, -3, -2,  5, -1, -3, -1,  0, -1, -3, -2, -2,  0,  1, -1, -4 ],
      [-1 ,-1 ,-2 ,-3 ,-1,  0, -2, -3, -2,  1,  2, -1,  5,  0, -2, -1, -1, -1, -1,  1, -3, -1, -1, -4 ],
      [-2 ,-3 ,-3 ,-3 ,-2, -3, -3, -3, -1,  0,  0, -3,  0,  6, -4, -2, -2,  1,  3, -1, -3, -3, -1, -4 ],
      [-1 ,-2 ,-2 ,-1 ,-3, -1, -1, -2, -2, -3, -3, -1, -2, -4,  7, -1, -1, -4, -3, -2, -2, -1, -2, -4 ],
      [ 1 ,-1 , 1 , 0 ,-1,  0,  0,  0, -1, -2, -2,  0, -1, -2, -1,  4,  1, -3, -2, -2,  0,  0,  0, -4 ],
      [ 0 ,-1 , 0 ,-1 ,-1, -1, -1, -2, -2, -1, -1, -1, -1, -2, -1,  1,  5, -2, -2,  0, -1, -1,  0, -4 ],
      [-3 ,-3 ,-4 ,-4 ,-2, -2, -3, -2, -2, -3, -2, -3, -1,  1, -4, -3, -2, 11,  2, -3, -4, -3, -2, -4 ],
      [-2, -2 ,-2 ,-3 ,-2, -1, -2, -3,  2, -1, -1, -2, -1,  3, -3, -2, -2,  2,  7, -1, -3, -2, -1, -4 ],
      [ 0 ,-3 ,-3 ,-3 ,-1, -2, -2, -3, -3,  3,  1, -2,  1, -1, -2, -2,  0, -3, -1,  4, -3, -2, -1, -4 ],
      [-2 ,-1 , 3 , 4 ,-3,  0,  1, -1,  0, -3, -4,  0, -3, -3, -2,  0, -1, -4, -3, -3,  4,  1, -1, -4 ],
      [-1 , 0 , 0 , 1, -3,  3,  4, -2,  0, -3, -3,  1, -1, -3, -1,  0, -1, -3, -2, -2,  1,  4, -1, -4 ],
      [ 0 ,-1 ,-1 ,-1, -2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -2,  0,  0, -2, -1, -1, -1, -1, -1, -4 ],
      [-4 ,-4, -4 ,-4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4,  1 ]]

def mk_dir(name_dir):
    if not(os.path.exists(name_dir)):
        os.mkdir(name_dir)
    name_dir += '/'
    return name_dir

def Simil_blosum(refeSeq,hitSeq):
    '''
    BLOSUM score
    '''
    Score = 0; score2 = 0
    for i in range(0,len(refeSeq)):
        ## blosum score
        pos1 = AAs.find(refeSeq[i])
        pos2 = AAs.find(hitSeq[i])
        Score = Score + blosum62[pos1][pos2]
        if(refeSeq[i] == hitSeq[i]):
            score2 += 1
    perScore = float(Score) / len(refeSeq)
    intScore = int(perScore * 100)
    return Score, intScore

## Clustering
def write_candidates(hits, hitList, outputdir, flank_size, loopSeq):
    outfile1 = outputdir + "ca_coor.npy"
    outfile2 = outputdir + "bb_coor.npy"
    outfile3 = outputdir + "resnr_coor.npy"
    outfile4 = outputdir + "seq_coor.npy"
    outcoor = np.zeros((len(hits),) + hits[0].ca.shape)
    bb_coor = np.zeros((len(hits),) + hits[0].bb.shape)
    residue_nb = []; res_seq = []
    nbHit = 0
    for hit in hits:
        if hit.valid: # and hit.clashes==0:
            hitSeq = hit.seq[flank_size:(len(hit.seq)-flank_size)]
            BlosumSimil, identity = Simil_blosum(loopSeq,hitSeq)
            if BlosumSimil > 0:
                hitLoopSeq = hit.seq[flank_size:(len(hit.seq)-flank_size)]
                CandLoopSeq = "".join(str(x) for x in hitLoopSeq)
                hitList.write("%s %s %d %d %d %f %f %f %d %d %d %s\n"%(hit.pdbcode, hit.chain, hit.model, hit.match_start, hit.match_end, hit.score, hit.rigidity, hit.rmsd, hit.clashes, BlosumSimil, identity, CandLoopSeq))
                outcoor[nbHit] = hit.ca
                bb_coor[nbHit] = hit.bb
                residue_nb.append(hit.resnrs)
                res_seq.append(hit.seq)
                nbHit += 1
    hitList.close()
    np.save(outfile1, outcoor[0:nbHit])
    np.save(outfile2, bb_coor[0:nbHit])
    np.save(outfile3, np.array(residue_nb))
    np.save(outfile4, np.array(res_seq))
    return 

## BCSearch
def BCrun(model_dir, loop_name, loopSeq, dbdir, dbname, th_BC, th_rigidity, th_RMSD):

    looplength = len(loopSeq)
    if th_BC == 0.0 :
        if looplength <= 8:
           th_BC =  0.9
        else:
           th_BC = 0.8
    sys.stderr.write( "Searching for loop size %d\n" % (looplength) )
    loop_dir = mk_dir(model_dir + loop_name)

    refe_pdb = PDB.PDB(loop_dir + "GappedPDB.pdb")
    refe_array = get_ca(refe_pdb)    
    flank1 = PDB.PDB(loop_dir + "flank1.pdb")
    flank2 = PDB.PDB(loop_dir + "flank2.pdb")
    flank1_ca = get_ca(flank1)
    flank2_ca = get_ca(flank2)
    flank_size = len(flank1)

    homolog_file = model_dir + "/homologs.list"
    homologs = [l.strip() for l in open(homolog_file)]
    library = PDBLibrary(dbdir, dbname)
    if len(homologs):
        library = library.exclude_codes(homologs)

    sys.stderr.write( "Searching parameters %f %f %f \n" % (th_BC, th_rigidity, th_RMSD) )

    num_cpu = 8
    run = BCLoopSearch()
    run.flank1 = flank1_ca
    run.flank2 = flank2_ca
    run.looplength = looplength
    run.minloopmatch = looplength
    run.maxloopgap = 0
    run.minBC = th_BC
    run.maxR = th_rigidity
    run.maxRMSD = th_RMSD
    run.library = library
    run.clash_body = refe_array
    run.maxhits = 1000000
    run.nthreads = num_cpu
    hits = run.run()
    codes = {}
    sys.stderr.write( "Found %d hits (size %d)\n" % (len(hits), looplength) )
    if len(hits) == 0:
        sys.stderr.write("no hit\n")
    else:
        outputdir = mk_dir(loop_dir + "BCSearch") 
        hitListname = outputdir + "/list_BC" + str(th_BC) + "_R" + str(th_rigidity) + ".txt"
        hitList = open(hitListname,'w')
        write_candidates(hits, hitList, outputdir, flank_size, loopSeq)
    return

def BCrun_thread(model_dir, dbdir, dbname, th_BC, th_rigidity, th_RMSD, Loop_info):
    loopSeq = []; loop_name = []
    nbLoop = len(Loop_info) - 1
    for k in range(nbLoop):
        LoopFileLine = Loop_info[k+1].split()
        loopSeq.append(LoopFileLine[1])
        loop_name.append("Loop%s_%s" %(LoopFileLine[0],LoopFileLine[1]))
        BCrun(model_dir, loop_name[k], loopSeq[k], dbdir, dbname, th_BC, th_rigidity, th_RMSD)
    '''
    threads = []
    for k in range(nbLoop):
        p = mp.Process(target=BCrun, args=(targetAddr, protName, loop_name[k], loopSeq[k], dbdir, dbname, th_BC, th_rigidity, th_RMSD,))
        p.start()
        threads.append(p)    
    for t in threads:
        t.join()
    '''
    return
    
def print_options(options):
    """
    @return: a string describing the options setup for the run
    """
    try:
        rs = ""
        for option in options.__dict__.keys():
            rs = "%s\n  %10s: %s" % (rs, str(option),getattr(options,option))
        return rs
    except:
        return ""


# -- ARGUMENTS --
def cmdLine():
    """
    Command line definition.

    @return: command line options and arguments (optionParser object)
    """

    parser = argparse.ArgumentParser()
    parser.add_argument("--target", dest="target", help="PDB to cyclize head to tail",
                        action="store", required=True, default = None)
    
    parser.add_argument("--wpath", dest="work_path", help="path to target (%s)" % DFLT_WORK_PATH,
                        action="store", default=DFLT_WORK_PATH)
    
    parser.add_argument("--flank_sze", dest="flank_sze", type = int, help="Flank size (%d)" % DFLT_FLANK_SZE,
                        action="store", default=DFLT_FLANK_SZE)

    parser.add_argument("--pdb_path", dest="pdb_path", help="path to indexed PDB (%s)" % DFLT_PDB_PATH,
                        action="store", default=DFLT_PDB_PATH)
    
    parser.add_argument("--pdb_name", dest="pdb_name", help="name of indexed PDB (%s)" % DFLT_PDB_NAME,
                        action="store", default=DFLT_PDB_NAME)
    
    parser.add_argument("--bc_min", dest="bc_min", type = float, help="BC cut-off (minimum)",
                        action="store", default=None)
    
    parser.add_argument("--rigidity", dest="rigidity", type = float, help="Rigidity cut-off (maximum) (%f)" % DFLT_RIGIDITY_MAX,
                        action="store", default=DFLT_RIGIDITY_MAX)
    
    parser.add_argument("--rmsd_max", dest="rmsd_max", type = float, help="RMSD cut-off (maximum)(%f)" % DFLT_RMSD_MAX,
                        action="store", default=DFLT_RMSD_MAX)
    
    parser.add_argument("--run_mod", dest="run_mod", help="running mode (\"cluster\" or \"local\")(%s)" % DFLT_MODE,
                        action="store", default=DFLT_MODE)

    parser.add_argument("--l", dest="Lbl", help="the label for the output results.",
                        action="store", default=DFLT_LBL) 
            
    return parser


def argParse():
    """
    Check and parse arguments.
    """

    parser = cmdLine()

    try:
        options = parser.parse_args()
    except ValueError:
        parser.error("\nInvalid command line format !\n")


    if options.work_path[-1] != "/":
        options.work_path = options.work_path + "/"
    if options.pdb_path[-1] != "/":
        options.pdb_path = options.pdb_path + "/"
        
    print print_options(options)    
        
    return options


if __name__=="__main__":
    """
    Parse the command line and launch the main purpose.
    """

    options = argParse()

    targetAddr = options.work_path
    protName   = options.target
    pdbdir     = options.pdb_path
    pdbName    = options.pdb_name
    BCth       = options.bc_min
    RigidityTh = options.rigidity
    RMSDth     = options.rmsd_max
    mode       = options.run_mod
    out_label  = options.Lbl

    model_dir = targetAddr + PROT_DIR + "/"
    Loop_info  = open(targetAddr + out_label + "_loops.log","r").readlines()

    if mode == "cluster":
       nb_line = int(os.environ['TASK_ID'])
       if Loop_info[nb_line].startswith("#"): 
          sys.stderr.write("Error reading loops info file\n")
       LoopFileLine = Loop_info[nb_line].split()
       LoopSeq = LoopFileLine[1]
       Loop_Name = "Loop" + LoopFileLine[0] + "_" + LoopSeq
       BCrun(model_dir, Loop_Name, LoopSeq, pdbdir, pdbName, BCth, RigidityTh, RMSDth)
    else:
       BCrun_thread(model_dir, pdbdir, pdbName, BCth, RigidityTh, RMSDth, Loop_info)

