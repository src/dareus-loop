#!/usr/bin/env python2.7

"""
author: Yasaman Karami
5 July 2018
MTi
"""

###########################
###  L I B R A R I E S  ###
###########################
import numpy as np
import BC
from BC import PDBLibrary
from BC import BCLoopSearch
from PyPDB import PyPDB as PDB
import sys, os
from rmsd import *
from scipy.cluster.hierarchy import fcluster
from scipy.cluster.hierarchy import dendrogram, linkage
import scipy.cluster.hierarchy as hcl
from scipy.spatial.distance import squareform
import argparse
import multiprocessing as mp
import warnings
warnings.filterwarnings("always")

DFLT_WORK_PATH     = "./"
DFLT_BANK          = "/scratch/banks/bc_banks"
DFLT_PDB_PATH      = DFLT_BANK + "/pdbChainID/"
DFLT_PDB_NAME      = "pdbChainID"
DFLT_RIGIDITY_MAX  = 3.
DFLT_FLANK_SZE     = 4
DFLT_RMSD_MAX      = 4.
DFLT_MODE          = "local"  #else "cluster"
AAs                = "ARNDCQEGHILKMFPSTWYVBZX*"
PROT_DIR           = "DaReUS_Loop"
DFLT_LBL           = "dareus"
#########################################################################
#########################################################################
def mk_dir(name_dir):
    if not(os.path.exists(name_dir)):
        os.mkdir(name_dir)
    name_dir += '/'
    return name_dir

def sort(clust,Similarity,th_rigidity,flank_size,th_BC):
    '''
    choosing one candidate with the highest sequence similarity from each cluster
    storing the members of each cluster
    '''
    ClustTick = np.zeros(len(clust), dtype='int32') 
    for j in range(0,max(clust)):
        clustInd = np.argwhere(clust == (j+1))
        Maxx = np.max(Similarity[clust == (j+1)])
        Maxx_Ind = np.argwhere(Similarity[clustInd] == Maxx)[:,0]
        Ind_clust_max = clustInd[Maxx_Ind][:,0]
        #Sorted_Ind = np.argsort(hit_pdb_name[Ind_clust_max])
        #index = int(clustInd[Sorted_Ind[0]])
        index = Ind_clust_max[0]
        ClustTick[index] = j+1
    return ClustTick

def cluster(RMSD,threshold):
    ### 3D clustering of the candidates based on the RMSD
    Z = hcl.linkage(squareform(RMSD))
    clusters = np.array(fcluster(Z,threshold, criterion='distance'))
    return clusters

def divide_cluster(loopSeq, Similarity, hits_bb, threshold, chunksize,flank_size,th_BC,th_rigidity):
   
    nbHits = len(Similarity)
    ClustTick = np.zeros(nbHits, dtype='int32')
    nchunks = int(float(nbHits)/chunksize+0.9999999)

    for i in range(nchunks):
        start1 = i*chunksize
        end1 = (i+1) * chunksize
        if end1 > nbHits: 
            end1 = nbHits
        RMSDmat = RMSDall(hits_bb[start1:end1],flank_size,len(loopSeq))
        clust = cluster(RMSDmat,threshold)
        subClustTick = sort(clust,Similarity[start1:end1],th_rigidity,flank_size,th_BC)
        ClustTick[start1:end1] = subClustTick

        ind = 0; new_ind = []
    for i in range(0,nbHits):
        if ClustTick[ind] > 0:
            new_ind.append(i)
        ind += 1
    return ClustTick, new_ind[0:ind]

def Clustering(model_dir, loop_name, th_BC, th_rigidity, flank_size, loopSeq):
    nbCluster = 5000; threshold = 1
    looplength = len(loopSeq)
    if th_BC == 0.0 :
        if looplength <= 8:
           th_BC =  0.9
        else:
           th_BC = 0.8
    loop_dir = model_dir + loop_name + "/"
    outputdir = loop_dir + "BCSearch/" 
    pdb_dir = mk_dir(outputdir + "PDB_files")

    final_list = open(outputdir + "/BCLoopCluster_Final_BC.txt", "w")

    hitListname = outputdir + "/list_BC" + str(th_BC) + "_R" + str(th_rigidity) + ".txt"
    hitListLines = open(hitListname,"r").readlines()
    BC_list = [l.split() for l in open(hitListname)]
    Similarity = np.array([int(ll[10]) for ll in BC_list])
    nbHits = len(Similarity)

    hits_ca = np.load(outputdir + "ca_coor.npy")
    hits_bb = np.load(outputdir + "bb_coor.npy")
    hits_resnr = np.load(outputdir + "resnr_coor.npy")
    hits_seq = np.load(outputdir + "seq_coor.npy")

    if nbHits == 1:
        final_list.write(hitListLines[0])
    else:
        ### first round of clustering
        ClustTick1, new_ind = divide_cluster(loopSeq, Similarity, hits_ca, threshold, nbCluster, flank_size, th_BC, th_rigidity)
        Similarity = Similarity[new_ind]
        hits_ca = hits_ca[new_ind]
        ### second round of clustering
        ClustTick2, new_ind = divide_cluster(loopSeq, Similarity, hits_ca, threshold, nbCluster, flank_size, th_BC, th_rigidity)
        ### writing the results
        j=0
        for i in range(nbHits):
            if ClustTick1[i] > 0:
                if ClustTick2[j] > 0:
                    final_list.write(hitListLines[i])
                    pdbtxt = hit_pdb(hits_bb[i], hits_resnr[i], hits_seq[i])
                    name = BC_list[i][0] + "-" + BC_list[i][1] + "-" + BC_list[i][2] + "-" + BC_list[i][3] + "-" + BC_list[i][4]
                    oname = pdb_dir + "/" + name + ".pdb"
                    open(oname, "w").write(pdbtxt)                
                j += 1
    return

                
def BCrun_thread(model_dir, th_BC, th_rigidity, Loop_info, flank_size):         
    loopSeq = []; loop_name = []
    nbLoop = len(Loop_info) - 1
    for k in range(nbLoop):
        LoopFileLine = Loop_info[k+1].split()
        loopSeq.append(LoopFileLine[1])
        loop_name.append("Loop%s_%s" %(LoopFileLine[0],LoopFileLine[1]))
        Clustering(model_dir, loop_name[k], th_BC, th_rigidity, flank_size, loopSeq[k])
    '''
    threads = []
    for k in range(nbLoop):
        p = mp.Process(target=Clustering, args=(targetAddr, protName, loop_name[k], th_BC, th_rigidity, flank_size, loopSeq[k],))
        p.start()
        threads.append(p)    
    for t in threads:
        t.join()
    '''
    return
    
def print_options(options):
    """
    @return: a string describing the options setup for the run
    """
    try:
        rs = ""
        for option in options.__dict__.keys():
            rs = "%s\n  %10s: %s" % (rs, str(option),getattr(options,option))
        return rs
    except:
        return ""


# -- ARGUMENTS --
def cmdLine():
    """
    Command line definition.

    @return: command line options and arguments (optionParser object)
    """

    parser = argparse.ArgumentParser()
    parser.add_argument("--target", dest="target", help="PDB to cyclize head to tail",
                        action="store", required=True, default = None)
    
    parser.add_argument("--wpath", dest="work_path", help="path to target (%s)" % DFLT_WORK_PATH,
                        action="store", default=DFLT_WORK_PATH)
    
    parser.add_argument("--flank_sze", dest="flank_sze", type = int, help="Flank size (%d)" % DFLT_FLANK_SZE,
                        action="store", default=DFLT_FLANK_SZE)

    parser.add_argument("--pdb_path", dest="pdb_path", help="path to indexed PDB (%s)" % DFLT_PDB_PATH,
                        action="store", default=DFLT_PDB_PATH)
    
    parser.add_argument("--pdb_name", dest="pdb_name", help="name of indexed PDB (%s)" % DFLT_PDB_NAME,
                        action="store", default=DFLT_PDB_NAME)
    
    parser.add_argument("--bc_min", dest="bc_min", type = float, help="BC cut-off (minimum)",
                        action="store", default=None)
    
    parser.add_argument("--rigidity", dest="rigidity", type = float, help="Rigidity cut-off (maximum) (%f)" % DFLT_RIGIDITY_MAX,
                        action="store", default=DFLT_RIGIDITY_MAX)
    
    parser.add_argument("--rmsd_max", dest="rmsd_max", type = float, help="RMSD cut-off (maximum)(%f)" % DFLT_RMSD_MAX,
                        action="store", default=DFLT_RMSD_MAX)
    
    parser.add_argument("--run_mod", dest="run_mod", help="running mode (\"cluster\" or \"local\")(%s)" % DFLT_MODE,
                        action="store", default=DFLT_MODE) 

    parser.add_argument("--l", dest="Lbl", help="the label for the output results.",
                        action="store", default=DFLT_LBL)
            
    return parser


def argParse():
    """
    Check and parse arguments.
    """

    parser = cmdLine()

    try:
        options = parser.parse_args()
    except ValueError:
        parser.error("\nInvalid command line format !\n")


    if options.work_path[-1] != "/":
        options.work_path = options.work_path + "/"
    if options.pdb_path[-1] != "/":
        options.pdb_path = options.pdb_path + "/"
        
    print print_options(options)    
        
    return options


if __name__=="__main__":
    """
    Parse the command line and launch the main purpose.
    """

    options = argParse()

    targetAddr = options.work_path
    protName   = options.target
    pdbdir     = options.pdb_path
    pdbName    = options.pdb_name
    BCth       = options.bc_min
    RigidityTh = options.rigidity
    RMSDth     = options.rmsd_max
    mode       = options.run_mod
    flank_size = options.flank_sze
    out_label  = options.Lbl

    model_dir = targetAddr + PROT_DIR + "/"
    Loop_info  = open(targetAddr + out_label + "_loops.log","r").readlines()

    if mode == "cluster":
       nb_line = int(os.environ['TASK_ID'])
       if Loop_info[nb_line].startswith("#"): 
          sys.stderr.write("Error reading loops info file\n")
       LoopFileLine = Loop_info[nb_line].split()
       LoopSeq = LoopFileLine[1]
       Loop_Name = "Loop" + LoopFileLine[0] + "_" + LoopSeq
       Clustering(model_dir, Loop_Name, BCth, RigidityTh, flank_size, LoopSeq)
    else:
       BCrun_thread(model_dir, BCth, RigidityTh, Loop_info, flank_size)
